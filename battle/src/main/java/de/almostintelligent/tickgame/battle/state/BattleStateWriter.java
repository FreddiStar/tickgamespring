package de.almostintelligent.tickgame.battle.state;

import de.almostintelligent.tickgame.battle.Battle;

public interface BattleStateWriter {

    void write(Battle battle);

}

package de.almostintelligent.tickgame.battle.state;

import de.almostintelligent.tickgame.battle.Battle;

public interface BattleStateReader {

    void setupBattleField(Battle battle);

    void setupUnits(Battle battle);

    void setupStructures(Battle battle);

}

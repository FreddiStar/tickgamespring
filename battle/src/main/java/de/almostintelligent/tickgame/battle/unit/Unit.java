package de.almostintelligent.tickgame.battle.unit;

import de.almostintelligent.tickgame.battle.gear.armor.Armor;
import de.almostintelligent.tickgame.battle.gear.shield.Shield;
import de.almostintelligent.tickgame.battle.gear.weapon.Weapon;
import de.almostintelligent.tickgame.battle.object.BattleFieldObject;

import java.util.List;

public class Unit extends BattleFieldObject {

    private Weapon primaryWeapon;
    private Weapon secondaryWeapon;
    private Shield shield;
    private Armor armor;
    private List<Tag> tags;

}



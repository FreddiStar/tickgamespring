package de.almostintelligent.tickgame.battle;

import de.almostintelligent.tickgame.battle.state.BattleStateReader;
import de.almostintelligent.tickgame.battle.state.BattleStateWriter;

import java.util.List;

public class BattleUtils {

    public static void readState(Battle battle, BattleStateReader battleStateReader) {
        battleStateReader.setupBattleField(battle);
        battleStateReader.setupStructures(battle);
        battleStateReader.setupUnits(battle);
    }

    public static void writeState(Battle battle, List<BattleStateWriter> battleStateWriters) {
        battleStateWriters.forEach(battleStateWriter -> battleStateWriter.write(battle));
    }
}
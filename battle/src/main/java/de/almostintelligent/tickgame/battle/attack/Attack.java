package de.almostintelligent.tickgame.battle.attack;

public class Attack {

    public static final int SUCCESS_VALUE = 20;

    /**
     * @param dice Rolled value
     * @param ac   Armor Class
     * @param ab   Attack Bonus
     * @param e    Encumbrance
     * @return true if hit was successful
     */
    public static boolean success(int dice, int ac, int ab, int e) {
        return dice + ac + ab - e >= SUCCESS_VALUE;
    }

}

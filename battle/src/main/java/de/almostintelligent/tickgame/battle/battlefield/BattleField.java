package de.almostintelligent.tickgame.battle.battlefield;

public class BattleField {

    private Integer width;
    private Integer height;

    public BattleField(Integer width, Integer height) {
        this.width = width;
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }
}

package de.almostintelligent.tickgame.battle.gear.weapon;

import de.almostintelligent.tickgame.battle.Dice;
import de.almostintelligent.tickgame.battle.gear.weapon.type.WeaponType;

/**
 * Created by frederiktrojahn on 14.03.16.
 */
public class Weapon {

    public enum Hands {
        ONE_HANDED, TWO_HANDED
    }

    private WeaponType weaponType;
    private String name;
    private Dice damage;
    private Hands hands;

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dice getDamage() {
        return damage;
    }

    public void setDamage(Dice damage) {
        this.damage = damage;
    }

    public Hands getHands() {
        return hands;
    }

    public void setHands(Hands hands) {
        this.hands = hands;
    }
}

package de.almostintelligent.tickgame.battle.command;

public interface Command {

    void execute();

}

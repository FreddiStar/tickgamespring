package de.almostintelligent.tickgame.battle;

public class Dice {

    private Integer count;
    private Integer value;

    public Dice(Integer count, Integer value) {
        this.count = count;
        this.value = value;
    }
}

package de.almostintelligent.tickgame.battle;

import de.almostintelligent.tickgame.battle.battlefield.BattleField;
import de.almostintelligent.tickgame.battle.object.BattleFieldObject;

import java.util.List;

public class Battle {

    private BattleField battleField;
    private List<BattleFieldObject> battleFieldObjects;

    public BattleField getBattleField() {
        return battleField;
    }

    public void setBattleField(BattleField battleField) {
        this.battleField = battleField;
    }

    public void fight() {

    }
}

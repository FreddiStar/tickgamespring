package de.almostintelligent.common.math;

public class LFSR {

    private long value;

    public LFSR(int seed) {
        this.value = seed;
    }

    public long next() {
        long bit = value >> 63 ^ value >> 62 ^ value >> 60 ^ value >> 59 & 1;
        value = value >> 1 | bit << 63;
        return value;
    }
}

package de.almostintelligent.common;

public interface Builder<T> {
    T build();
}

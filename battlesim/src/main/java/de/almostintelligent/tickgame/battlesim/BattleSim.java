package de.almostintelligent.tickgame.battlesim;

import de.almostintelligent.tickgame.battle.Battle;
import de.almostintelligent.tickgame.battle.BattleUtils;
import de.almostintelligent.tickgame.battle.state.BattleStateReader;
import de.almostintelligent.tickgame.battle.state.BattleStateWriter;

import java.util.ArrayList;
import java.util.List;

public class BattleSim implements Runnable {

    public BattleSim(String[] args) {

    }

    @Override
    public void run() {
        BattleStateReader jsonBattleStateReader = new ManualBattleStateReader();

        List<BattleStateWriter> battleStateWriters = new ArrayList<>();
        JSONBattleStateWriter jsonBattleStateWriter = new JSONBattleStateWriter();
        SVGBattleStateWriter svgBattleStateWriter = new SVGBattleStateWriter("out.html");

        battleStateWriters.add(jsonBattleStateWriter);
        battleStateWriters.add(svgBattleStateWriter);

        Battle battle = new Battle();

        BattleUtils.readState(battle, jsonBattleStateReader);

        battle.fight();

        BattleUtils.writeState(battle, battleStateWriters);
    }

    public static void main(String[] args) {
        BattleSim sim = new BattleSim(args);
        sim.run();
    }
}

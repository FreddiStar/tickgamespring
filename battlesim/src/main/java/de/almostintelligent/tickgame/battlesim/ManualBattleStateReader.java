package de.almostintelligent.tickgame.battlesim;

import de.almostintelligent.tickgame.battle.Battle;
import de.almostintelligent.tickgame.battle.battlefield.BattleField;
import de.almostintelligent.tickgame.battle.state.BattleStateReader;

public class ManualBattleStateReader implements BattleStateReader {
    @Override
    public void setupBattleField(Battle battle) {
        BattleField battleField = new BattleField(50, 50);
        battle.setBattleField(battleField);
    }

    @Override
    public void setupUnits(Battle battle) {

    }

    @Override
    public void setupStructures(Battle battle) {

    }
}

package de.almostintelligent.tickgame.battlesim;

import de.almostintelligent.tickgame.battle.Battle;
import de.almostintelligent.tickgame.battle.state.BattleStateReader;

public class JSONBattleStateReader implements BattleStateReader {

    @Override
    public void setupBattleField(Battle battle) {

    }

    @Override
    public void setupUnits(Battle battle) {

    }

    @Override
    public void setupStructures(Battle battle) {

    }
}

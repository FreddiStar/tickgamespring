package de.almostintelligent.tickgame.battlesim;

import de.almostintelligent.tickgame.battle.Battle;
import de.almostintelligent.tickgame.battle.battlefield.BattleField;
import de.almostintelligent.tickgame.battle.state.BattleStateWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SVGBattleStateWriter implements BattleStateWriter {

    private String filename;
    private int fieldSize = 15;

    public SVGBattleStateWriter(String filename) {
        this.filename = filename;
    }

    @Override
    public void write(Battle battle) {
        try {
            File f = new File(filename);

            if (!f.exists()) {
                f.createNewFile();
            }

            FileOutputStream fileOutputStream = new FileOutputStream(f);
            StringBuilder builder = new StringBuilder();

            BattleField battleField = battle.getBattleField();

            writeHeader(builder, battleField);

            writeBattleField(builder, battleField);

            writeFooter(builder);

            fileOutputStream.write(builder.toString().getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeBattleField(StringBuilder builder, BattleField battleField) {
        for (int x = 0; x < battleField.getWidth(); ++x) {
            for (int y = 0; y < battleField.getWidth(); ++y) {
                builder.append(String.format("<rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" stroke=\"black\" stroke-width=\"1\" fill=\"green\" />\n", x * fieldSize, y * fieldSize, fieldSize, fieldSize));
            }
        }
    }

    private void writeFooter(StringBuilder builder) {
        builder.append("</svg>\n");
        builder.append("</body>\n" +
                "</html>");
    }

    private void writeHeader(StringBuilder builder, BattleField battleField) {
        builder.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n");
        builder.append(String.format("<svg width=\"%d\" height=\"%d\">\n", battleField.getWidth() * fieldSize, battleField.getHeight() * fieldSize));
    }
}

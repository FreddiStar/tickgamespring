import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('game', function() {
    this.route('overview');
    this.route('building');
    this.route('research');
    this.route('units');
    this.route('army');
    this.route('alliance');
    this.route('map');
  });
  this.route('frontpage', function() {
    this.route('blog');
    this.route('login');
    this.route('register');
    this.route('about');
  });
  this.route('error', { path: '/error/:error' });
});

export default Router;

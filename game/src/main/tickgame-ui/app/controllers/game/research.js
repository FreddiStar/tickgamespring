import Ember from 'ember';

export default Ember.Controller.extend({
    researches: Ember.computed.filter('model.constructibles', function(constructible, index, array) {
        return constructible.get('type') === 'RESEARCH';
    }),

    playerResearches: Ember.computed('researches', 'playerHasResearch', function() {
            var that = this;
            var result = [];
            this.get('researches').forEach(function(b) {
                var foundLevel = 0;
                that.get('model.playerHasResearch').forEach(function(vhb) {
                    if (vhb.get('constructible').get('id') === b.get('id')) {
                        foundLevel = vhb.get('level');
                        return;
                    }
                });

                result.push({
                     research: b,
                     level: foundLevel
                 });
            });
            return result;
        })
});


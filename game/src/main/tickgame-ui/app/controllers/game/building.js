import Ember from 'ember';

export default Ember.Controller.extend({
    buildings: Ember.computed.filter('model.constructibles', function(constructible, index, array) {
        return constructible.get('type') === 'BUILDING';
    }),

    villageBuildings: Ember.computed('buildings', 'villageHasBuildings', function() {
        var that = this;
        var result = [];
        this.get('buildings').forEach(function(b) {
            var foundLevel = 0;
            that.get('model.villageHasBuildings').forEach(function(vhb) {
                if (vhb.get('constructible').get('id') === b.get('id')) {
                    foundLevel = vhb.get('level');
                    return;
                }
            });

            result.push({
                 building: b,
                 level: foundLevel
             });
        });
        return result;
    }),

    actions: {
        build(id) {
            let constructible = this.store.peekRecord('constructible', id);
            let construction = this.store.createRecord('construction', { constructible: constructible });
            construction.save();
        }
    }
});

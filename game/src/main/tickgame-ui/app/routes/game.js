import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    model() {
        return Ember.RSVP.hash({
            player: this.store.findAll('current-player').then(function(current) {
                return current.get('firstObject');
            })
        });
    }
});

import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        return Ember.RSVP.hash({
            constructibles: this.store.findAll('constructible'),
            constructions: this.store.query('construction', {'type': 'building'}),
            villageHasBuildings: this.store.findAll('village-has-building')
        });
    }
});

import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        return Ember.RSVP.hash({
                constructibles: this.store.findAll('constructible'),
                constructions: this.store.query('construction', {'type': 'research'}),
                playerHasResearch: this.store.findAll('player-has-research')
            });
    }
});

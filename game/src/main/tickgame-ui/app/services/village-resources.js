import Ember from 'ember';

export default Ember.Service.extend({
    init: function() {
        this.set('wood', 0);
        this.set('stone', 0);
        this.set('coal', 0);
        this.set('iron', 0);
    }
});

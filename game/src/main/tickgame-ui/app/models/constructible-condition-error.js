import DS from 'ember-data';

export default DS.Model.extend({
    message: DS.attr('string'),
    constructible: DS.belongsTo('constructible')
});

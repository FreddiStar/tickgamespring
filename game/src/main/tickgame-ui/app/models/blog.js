import DS from 'ember-data';

export default DS.Model.extend({
  createdAt: DS.attr('number'),
  updatedAt: DS.attr('number'),
  title: DS.attr('string'),
  body: DS.attr('string'),
  comments: DS.hasMany('blog-comment')
});

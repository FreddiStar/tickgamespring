import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    value: DS.attr('number'),
    growth: DS.attr('number'),
    growthType: DS.attr('string'),
    constructible: DS.belongsTo('constructible')
});

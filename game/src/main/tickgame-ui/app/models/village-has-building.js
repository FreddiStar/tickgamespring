import DS from 'ember-data';

export default DS.Model.extend({
    level: DS.attr('number'),
    village: DS.belongsTo('village'),
    constructible: DS.belongsTo('constructible')
});

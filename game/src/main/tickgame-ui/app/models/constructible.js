import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
    type: DS.attr('string'),
    datas: DS.hasMany('constructible-datum'),
    conditionErrors: DS.hasMany('constructible-condition-error'),
    villageHasBuildings: DS.hasMany('village-has-building'),
    cost: Ember.computed.filter('datas', function(datum, index, array) {
        return datum.get('name').lastIndexOf('COST_') === 0;
    }),
});

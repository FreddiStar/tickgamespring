import DS from 'ember-data';

export default DS.Model.extend({
  constructible: DS.belongsTo('constructible'),
  dependency: DS.belongsTo('constructible'),
  level: DS.attr('number')
});

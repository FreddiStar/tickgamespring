import DS from 'ember-data';

export default DS.Model.extend({
    amount: DS.attr('number'),
    village: DS.belongsTo('village'),
    resource: DS.belongsTo('resource')
});

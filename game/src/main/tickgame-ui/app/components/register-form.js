import Ember from 'ember';

export default Ember.Component.extend({

    actions: {
        register() {
            var that = this;
            let { email, password, password_repeat } = this.getProperties('email', 'password', 'password_repeat');
            var registerData = {
                email: email,
                password: password,
                passwordRepeat: password_repeat
            };
            Ember.$.ajax({
                url: "/api/register",
                data: JSON.stringify(registerData),
                type: 'POST',
                contentType: 'application/json;charset=utf-8',
                success: function(data, status, jqXHR) {
                    that.setProperties({
                                           email: '',
                                           password: '',
                                           password_repeat: ''
                                       });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.setProperties({
                                           password: '',
                                           password_repeat: ''
                                       });
                }});
        }
    }

});

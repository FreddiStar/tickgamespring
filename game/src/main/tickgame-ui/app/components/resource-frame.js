import Ember from 'ember';

export default Ember.Component.extend({
    session: Ember.inject.service('session'),
    villageResources: Ember.inject.service('village-resources')
});

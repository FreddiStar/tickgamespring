import Ember from 'ember';

export function truncate(params, hash) {
    var limit = hash.limit || 50;
    var text = params[0];
    if (text.length > limit) {
        text = text.substr(0, limit - 3) + "...";
    }
    return text;
}

export default Ember.Helper.helper(truncate);

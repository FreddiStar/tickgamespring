import Ember from 'ember';

export function valueGrowth(params, hash) {
    if (hash.type === 'EXPONENTIAL') {
        return hash.value * Math.pow(hash.growth, hash.level);
    } else {
        return hash.growth * hash.evel + hash.value;
    }
}

export default Ember.Helper.helper(valueGrowth);

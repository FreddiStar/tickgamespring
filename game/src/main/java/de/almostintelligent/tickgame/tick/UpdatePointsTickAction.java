package de.almostintelligent.tickgame.tick;

import de.almostintelligent.tickgame.service.PlayerService;
import de.almostintelligent.tickgame.service.ScoreService;
import de.almostintelligent.tickgame.service.TickService;
import org.springframework.beans.factory.annotation.Autowired;

@RegisterAsTickAction
public class UpdatePointsTickAction implements TickAction {


    private ScoreService scoreService;

    @Autowired
    public UpdatePointsTickAction(PlayerService playerService,
                                  ScoreService scoreService) {
        this.scoreService = scoreService;
    }

    public void doAction() {
        scoreService.updateScore();
    }

    public Integer weight() {
        return TickService.TICK_ACTION_WEIGHT_UPATE_POINTS;
    }

    public String name() {
        return UpdatePointsTickAction.class.getSimpleName();
    }
}

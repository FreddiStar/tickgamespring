package de.almostintelligent.tickgame.tick;

import de.almostintelligent.tickgame.model.PlayerHasResearch;
import de.almostintelligent.tickgame.model.VillageHasBuilding;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.repository.ConstructionRepository;
import de.almostintelligent.tickgame.repository.PlayerHasResearchRepository;
import de.almostintelligent.tickgame.service.TickService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@RegisterAsTickAction
public class ResearchConstructionTickAction implements TickAction {

    Logger logger = LoggerFactory.getLogger(ResearchConstructionTickAction.class.getSimpleName());

    private ConstructionRepository constructionRepository;
    private PlayerHasResearchRepository playerHasResearchRepository;

    @Autowired
    public ResearchConstructionTickAction(ConstructionRepository constructionRepository,
                                          PlayerHasResearchRepository playerHasResearchRepository) {
        this.constructionRepository = constructionRepository;
        this.playerHasResearchRepository = playerHasResearchRepository;
    }

    @Transactional
    public void doAction() {
        constructionRepository.getConstructionsByType(ConstructibleType.RESEARCH).forEach(construction -> {
            construction.setTicksLeft(construction.getTicksLeft() - 1);

            if (construction.getTicksLeft() == 0) {
                Map<String, PlayerHasResearch> playerHasResearchMap = construction.getPlayer().getPlayerHasResearchesMap();
                if (!playerHasResearchMap.containsKey(construction.getConstructible().getId())) {
                    PlayerHasResearch playerHasResearch = new PlayerHasResearch();
                    playerHasResearch.setLevel(1L);
                    playerHasResearch.setConstructible(construction.getConstructible());
                    playerHasResearch.setPlayer(construction.getPlayer());
                    playerHasResearch = playerHasResearchRepository.save(playerHasResearch);
                } else {
                    PlayerHasResearch playerHasResearch = playerHasResearchMap.get(construction.getConstructible().getId());
                    playerHasResearch.setLevel(playerHasResearch.getLevel() + 1);
                    playerHasResearch = playerHasResearchRepository.save(playerHasResearch);
                }

                constructionRepository.delete(construction);
            } else {
                constructionRepository.save(construction);
            }
        });

    }

    public Integer weight() {
        return TickService.TICK_ACTION_WEIGHT_RESEARCH_CONSTRUCTION;
    }

    public String name() {
        return ResearchConstructionTickAction.class.getSimpleName();
    }
}

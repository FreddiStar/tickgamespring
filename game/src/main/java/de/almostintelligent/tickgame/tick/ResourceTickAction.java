package de.almostintelligent.tickgame.tick;

import de.almostintelligent.tickgame.datatypes.ResourcesByName;
import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.VillageHasResource;
import de.almostintelligent.tickgame.repository.PlayerRepository;
import de.almostintelligent.tickgame.repository.VillageHasBuildingRepository;
import de.almostintelligent.tickgame.repository.VillageRepository;
import de.almostintelligent.tickgame.service.TickService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@RegisterAsTickAction
public class ResourceTickAction implements TickAction {

    private PlayerRepository playerRepository;
    private VillageRepository villageRepository;

    @Autowired
    public ResourceTickAction(PlayerRepository playerRepository,
                              VillageRepository villageRepository) {
        this.playerRepository = playerRepository;
        this.villageRepository = villageRepository;
    }

    @Transactional
    public void doAction() {
        villageRepository.findAll().forEach(village ->
        {
            ResourcesByName resourcesByName = ResourcesByName.fromVillage(village);
            village.getVillageHasBuildings().stream().forEach(villageHasBuilding -> {
                Constructible constructible = villageHasBuilding.getConstructible();
                constructible.getConstructibleDatas().values().stream().forEach(data ->
                {
                    if (data.getName().startsWith("PROD_")) {
                        String resourceName = data.getName().split("_")[1].toLowerCase();
                        Double amount = resourcesByName.get(resourceName) + data.grow(villageHasBuilding.getLevel());
                        resourcesByName.put(resourceName, amount);
                    }
                });
            });

            for (VillageHasResource villageHasResource : village.getVillageHasResources()) {
                villageHasResource.setAmount(resourcesByName.get(villageHasResource.getResource().getName()));
            }

            villageRepository.save(village);
        });
    }

    public Integer weight() {
        return TickService.TICK_ACTION_WEIGHT_RESOURCES;
    }

    @Override
    public String name() {
        return ResourceTickAction.class.getSimpleName();
    }
}

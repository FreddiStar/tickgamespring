package de.almostintelligent.tickgame.tick;

public interface TickAction {

    void doAction();

    Integer weight();

    String name();

}

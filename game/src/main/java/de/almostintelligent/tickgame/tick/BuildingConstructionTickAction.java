package de.almostintelligent.tickgame.tick;

import de.almostintelligent.tickgame.model.VillageHasBuilding;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.repository.ConstructionRepository;
import de.almostintelligent.tickgame.repository.VillageHasBuildingRepository;
import de.almostintelligent.tickgame.service.TickService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@RegisterAsTickAction
public class BuildingConstructionTickAction implements TickAction {

    Logger logger = LoggerFactory.getLogger(BuildingConstructionTickAction.class.getSimpleName());

    private ConstructionRepository constructionRepository;
    private VillageHasBuildingRepository villageHasBuildingRepository;

    @Autowired
    public BuildingConstructionTickAction(ConstructionRepository constructionRepository,
                                          VillageHasBuildingRepository villageHasBuildingRepository) {
        this.constructionRepository = constructionRepository;
        this.villageHasBuildingRepository = villageHasBuildingRepository;
    }

    @Transactional
    public void doAction() {
        constructionRepository.getConstructionsByType(ConstructibleType.BUILDING).forEach(construction -> {
            construction.setTicksLeft(construction.getTicksLeft() - 1);

            if (construction.getTicksLeft() == 0) {
                Map<String, VillageHasBuilding> villageHasBuildings = construction.getVillage().getVillageHasBuildingsMap();
                if (!villageHasBuildings.containsKey(construction.getConstructible().getId())) {

                    VillageHasBuilding villageHasBuilding = new VillageHasBuilding();
                    villageHasBuilding.setConstructible(construction.getConstructible());
                    villageHasBuilding.setVillage(construction.getVillage());
                    villageHasBuilding.setLevel(1L);

                    villageHasBuilding = villageHasBuildingRepository.save(villageHasBuilding);

                    logger.error("Created constructible " + construction.getConstructible().getId() + " in village " + construction.getVillage().getName());

                } else {
                    VillageHasBuilding villageHasBuilding = villageHasBuildings.get(construction.getConstructible().getId());
                    villageHasBuilding.setLevel(villageHasBuilding.getLevel() + 1);

                    villageHasBuilding = villageHasBuildingRepository.save(villageHasBuilding);

                    logger.error("Upgraded constructible " + construction.getConstructible().getId() + " in village " + construction.getVillage().getName() + " to level " + villageHasBuilding.getLevel());
                }

                constructionRepository.delete(construction);
            } else {
                constructionRepository.save(construction);
            }
        });

    }

    public Integer weight() {
        return TickService.TICK_ACTION_WEIGHT_BUILDING_CONSTRUCTION;
    }

    public String name() {
        return BuildingConstructionTickAction.class.getSimpleName();
    }
}

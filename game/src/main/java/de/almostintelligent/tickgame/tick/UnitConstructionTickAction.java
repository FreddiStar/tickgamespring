package de.almostintelligent.tickgame.tick;

import de.almostintelligent.tickgame.model.PlayerHasUnit;
import de.almostintelligent.tickgame.model.VillageHasUnit;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.repository.ConstructionRepository;
import de.almostintelligent.tickgame.repository.PlayerHasUnitRepository;
import de.almostintelligent.tickgame.repository.VillageHasUnitRepository;
import de.almostintelligent.tickgame.service.TickService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@RegisterAsTickAction
public class UnitConstructionTickAction implements TickAction {

    Logger logger = LoggerFactory.getLogger(UnitConstructionTickAction.class.getSimpleName());

    private ConstructionRepository constructionRepository;
    private PlayerHasUnitRepository playerHasUnitRepository;
    private VillageHasUnitRepository villageHasUnitRepository;

    @Autowired
    public UnitConstructionTickAction(ConstructionRepository constructionRepository,
                                      PlayerHasUnitRepository playerHasUnitRepository,
                                      VillageHasUnitRepository villageHasUnitRepository) {
        this.constructionRepository = constructionRepository;
        this.playerHasUnitRepository = playerHasUnitRepository;
        this.villageHasUnitRepository = villageHasUnitRepository;
    }

    @Transactional
    public void doAction() {
        constructionRepository.getConstructionsByType(ConstructibleType.UNIT).forEach(construction -> {
            construction.setTicksLeft(construction.getTicksLeft() - 1);

            if (construction.getTicksLeft() == 0) {

                PlayerHasUnit playerHasUnit = new PlayerHasUnit();
                playerHasUnit.setPlayer(construction.getPlayer());
                playerHasUnit.setConstructible(construction.getConstructible());

                playerHasUnit = playerHasUnitRepository.save(playerHasUnit);

                VillageHasUnit villageHasUnit = new VillageHasUnit();
                villageHasUnit.setVillage(construction.getVillage());
                villageHasUnit.setPlayerHasUnit(playerHasUnit);

                villageHasUnitRepository.save(villageHasUnit);

                constructionRepository.delete(construction);
            } else {
                constructionRepository.save(construction);
            }
        });

    }

    public Integer weight() {
        return TickService.TICK_ACTION_WEIGHT_RESEARCH_CONSTRUCTION;
    }

    public String name() {
        return UnitConstructionTickAction.class.getSimpleName();
    }
}

package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;
import org.springframework.data.repository.CrudRepository;

public interface VillageRepository extends CrudRepository<Village, String> {

    Village findByPlayerAndFocused(Player player, Boolean focused);

}

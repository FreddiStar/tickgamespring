package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.UnitData;
import org.springframework.data.repository.CrudRepository;

public interface UnitDataRepository extends CrudRepository<UnitData, String> {
}

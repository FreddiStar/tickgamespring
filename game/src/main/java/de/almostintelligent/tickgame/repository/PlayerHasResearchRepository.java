package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.PlayerHasResearch;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PlayerHasResearchRepository extends CrudRepository<PlayerHasResearch, String> {

    @Query("SELECT pr FROM player_has_research pr WHERE pr.player = :player")
    List<PlayerHasResearch> getPlayerResearches(@Param("player") Player player);
}

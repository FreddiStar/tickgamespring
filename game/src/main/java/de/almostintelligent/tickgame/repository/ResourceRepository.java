package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Resource;
import org.springframework.data.repository.CrudRepository;

public interface ResourceRepository extends CrudRepository<Resource, String> {
}

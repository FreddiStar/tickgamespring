package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.VillageHasResource;
import org.springframework.data.repository.CrudRepository;

public interface VillageHasResourceRepository extends CrudRepository<VillageHasResource, String> {
}

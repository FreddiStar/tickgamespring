package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Blog;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BlogRepository extends PagingAndSortingRepository<Blog, String> {

    @Query("SELECT b FROM blog b ORDER BY b.createdAt DESC")
    List<Blog> getBlogsPage();

}

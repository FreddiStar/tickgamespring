package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Construction;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ConstructionRepository extends CrudRepository<Construction, String> {

    @Query("SELECT MAX(c.ticksLeft) FROM construction c LEFT JOIN c.constructible co WHERE c.village = :village AND c.player = :player AND co.type = :type")
    Long maxTicksLeft(@Param("village") Village village,
                      @Param("player") Player player,
                      @Param("type") ConstructibleType type);

    @Query("SELECT c FROM construction c LEFT JOIN c.constructible co WHERE c.village = :village AND c.player = :player AND co.type = :type ORDER BY c.ticksLeft ASC")
    List<Construction> getConstructionsByVillageAndPlayerAndType(@Param("village") Village village,
                                                                 @Param("player") Player player,
                                                                 @Param("type") ConstructibleType type);

    @Query("SELECT c FROM construction c LEFT JOIN c.constructible co WHERE c.village = NULL AND c.player = :player AND co.type = :type ORDER BY c.ticksLeft ASC")
    List<Construction> getConstructionsByPlayerAndType(@Param("player") Player player,
                                                       @Param("type") ConstructibleType type);

    @Query("SELECT c FROM construction c LEFT JOIN c.constructible co WHERE co.type = :type")
    List<Construction> getConstructionsByType(@Param("type") ConstructibleType type);

}

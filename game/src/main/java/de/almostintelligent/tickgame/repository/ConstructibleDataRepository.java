package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.ConstructibleDatum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConstructibleDataRepository extends CrudRepository<ConstructibleDatum, String> {
    @Query("SELECT d FROM constructible_datum d")
    List<ConstructibleDatum> getAll();
}

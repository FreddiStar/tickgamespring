package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.ArmorType;
import org.springframework.data.repository.CrudRepository;

public interface ArmorTypeRepository extends CrudRepository<ArmorType, String> {
}

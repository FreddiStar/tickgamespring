package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, String> {
}

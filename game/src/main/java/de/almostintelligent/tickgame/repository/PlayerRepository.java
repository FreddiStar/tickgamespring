package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Player;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<Player, String> {

    Player findOneByEmail(String email);


}

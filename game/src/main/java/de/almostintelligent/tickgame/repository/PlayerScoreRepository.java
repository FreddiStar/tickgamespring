package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Score;
import org.springframework.data.repository.CrudRepository;

public interface PlayerScoreRepository extends CrudRepository<Score, String> {
}

package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.BlogComment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BlogCommentRepository extends CrudRepository<BlogComment, String> {

    @Query("SELECT c FROM blog_comment c")
    List<BlogComment> getAll();

}

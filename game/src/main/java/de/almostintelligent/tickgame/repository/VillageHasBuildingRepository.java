package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.VillageHasBuilding;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VillageHasBuildingRepository extends CrudRepository<VillageHasBuilding, String> {

    List<VillageHasBuilding> findByVillage(Village village);

    VillageHasBuilding findByVillageAndConstructible(Village village, Constructible constructible);
}

package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.DamageType;
import org.springframework.data.repository.CrudRepository;

public interface DamageTypeRepository extends CrudRepository<DamageType, String> {
}

package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.PlayerActivation;
import org.springframework.data.repository.CrudRepository;

public interface PlayerActivationRepository extends CrudRepository<PlayerActivation, String> {
}

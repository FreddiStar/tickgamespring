package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConstructibleRepository extends CrudRepository<Constructible, String> {

    @Query("SELECT c FROM constructible c")
    List<Constructible> getAll();

    List<Constructible> findByTypeOrderBySortingAsc(ConstructibleType type);

}

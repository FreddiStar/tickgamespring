package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.VillageHasUnit;
import org.springframework.data.repository.CrudRepository;

public interface VillageHasUnitRepository extends CrudRepository<VillageHasUnit, String> {
}

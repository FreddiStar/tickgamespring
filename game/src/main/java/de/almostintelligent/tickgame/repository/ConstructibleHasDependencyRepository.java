package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.ConstructibleHasDependency;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConstructibleHasDependencyRepository extends CrudRepository<ConstructibleHasDependency, String> {
    @Query("SELECT d FROM constructible_has_dependency d")
    List<ConstructibleHasDependency> getAll();
}

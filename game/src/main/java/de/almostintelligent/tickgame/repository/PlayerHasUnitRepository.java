package de.almostintelligent.tickgame.repository;

import de.almostintelligent.tickgame.model.PlayerHasUnit;
import org.springframework.data.repository.CrudRepository;

public interface PlayerHasUnitRepository extends CrudRepository<PlayerHasUnit, String> {
}

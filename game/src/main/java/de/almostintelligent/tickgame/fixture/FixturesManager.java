package de.almostintelligent.tickgame.fixture;

import de.almostintelligent.tickgame.model.*;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.model.types.GrowthType;
import de.almostintelligent.tickgame.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class FixturesManager {

    Logger logger = LoggerFactory.getLogger("de.almostintelligent.tickgame.fixture");

    private final PlayerRepository playerRepository;
    private final ResourceRepository resourceRepository;
    private final DamageTypeRepository damageTypeRepository;
    private final ArmorTypeRepository armorTypeRepository;
    private final ConstructibleRepository constructibleRepository;
    private final UnitDataRepository unitDataRepository;
    private final ConstructibleHasDependencyRepository constructibleHasDependencyRepository;
    private final VillageRepository villageRepository;
    private final VillageHasResourceRepository villageHasResourceRepository;
    private final ConstructibleDataRepository constructibleDataRepository;

    // User
    private Player admin;
    private Player player;

    // Resources
    private Resource resource_stone;
    private Resource resource_wood;
    private Resource resource_coal;
    private Resource resource_iron;
    private Resource resource_food;

    // Armor and Damage types
    private ArmorType armor_leather;
    private ArmorType armor_plate;
    private ArmorType armor_chain;
    private ArmorType armor_fabric;
    private ArmorType armor_siege;

    private DamageType damage_blunt;
    private DamageType damage_arrow;
    private DamageType damage_bolt;
    private DamageType damage_sword;
    private DamageType damage_spear;
    private DamageType damage_axe;
    private DamageType damage_siege;

    // Buildings
    private Constructible building_lumberjack;
    private Constructible building_storage;
    private Constructible building_defenses;
    private Constructible building_barracks;
    private Constructible building_blacksmith;
    private Constructible building_stable;
    private Constructible building_market;
    private Constructible building_housing;
    private Constructible building_farm;
    private Constructible building_mine;
    private Constructible building_charcoalburner;
    private Constructible building_quarry;

    // Forschungen
    private Constructible research_crop_rotation;
    private Constructible research_siege_weapons;
    private Constructible research_chain_armor;
    private Constructible research_plate_armor;
    private Constructible research_leather_armor;
    private Constructible research_spears;
    private Constructible research_axes;
    private Constructible research_swords;
    private Constructible research_crossbows;
    private Constructible research_bow_and_arrow;


    // Einheiten
    private Constructible unit_catapult;
    private Constructible unit_ram;
    private Constructible unit_heave_cavalry;
    private Constructible unit_light_cavalry;
    private Constructible unit_scout;
    private Constructible unit_knight;
    private Constructible unit_spearman;
    private Constructible unit_axeman;
    private Constructible unit_swordsman;
    private Constructible unit_crossbowman;
    private Constructible unit_archer;
    private BlogRepository blogRepository;
    private RoleRepository roleRepository;

    public FixturesManager(PlayerRepository playerRepository,
                           ResourceRepository resourceRepository,
                           DamageTypeRepository damageTypeRepository,
                           ArmorTypeRepository armorTypeRepository,
                           ConstructibleRepository constructibleRepository,
                           UnitDataRepository unitDataRepository,
                           ConstructibleHasDependencyRepository constructibleHasDependencyRepository,
                           VillageRepository villageRepository,
                           VillageHasResourceRepository villageHasResourceRepository,
                           ConstructibleDataRepository constructibleDataRepository,
                           BlogRepository blogRepository, RoleRepository roleRepository) {
        this.playerRepository = playerRepository;
        this.resourceRepository = resourceRepository;
        this.damageTypeRepository = damageTypeRepository;
        this.armorTypeRepository = armorTypeRepository;
        this.constructibleRepository = constructibleRepository;
        this.unitDataRepository = unitDataRepository;
        this.constructibleHasDependencyRepository = constructibleHasDependencyRepository;
        this.villageRepository = villageRepository;
        this.villageHasResourceRepository = villageHasResourceRepository;
        this.constructibleDataRepository = constructibleDataRepository;
        this.blogRepository = blogRepository;
        this.roleRepository = roleRepository;
    }

    public void run() {

        createBlogs();

        createResources();
        setupPlayer();
        createDamageAndArmor();

        createBuildings();
        createResearches();
        createUnits();

        createBuildingDependencies();

    }

    private void createBlogs() {
        for (int i = 0; i < 5; ++i) {
            createBlog("Test title " + i, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. \n" +
                    "\n" +
                    "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. \n" +
                    "\n" +
                    "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. \n" +
                    "\n" +
                    "Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. \n" +
                    "\n" +
                    "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. ");
        }
    }

    private void createBlog(String title, String body) {

        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setBody(body);
        blog = blogRepository.save(blog);

        for (int i = 0; i < 4; ++i) {
            BlogComment comment = new BlogComment();
            comment.setComment("Some Comment " + i);
            comment.setBlog(blog);
            blog.getComments().add(comment);
        }

        blogRepository.save(blog);
    }

    private void createBuildingDependencies() {
        createConstructibleDependency(building_blacksmith, building_charcoalburner, 4);
        createConstructibleDependency(building_charcoalburner, building_lumberjack, 2);
    }

    private void createConstructibleDependency(Constructible constructible, Constructible dependsOn, int level) {
        ConstructibleHasDependency dependency = new ConstructibleHasDependency();
        dependency.setConstructible(constructible);
        dependency.setDependency(dependsOn);
        dependency.setLevel(level);

        constructibleHasDependencyRepository.save(dependency);
    }

    private void createUnits() {
        createArcher();
        createCrossbowman();
        createSwordsman();
        createAxeman();
        createSpearman();
        createKnight();
        createScout();
        createLightCavalry();
        createHeavyCavalry();
        createRam();
        createCatapult();
    }

    private void createCatapult() {
        unit_catapult = createConstructible("catapult", ConstructibleType.UNIT, 0);
        unit_catapult.setUnitData(createUnitData(1d, armor_siege, 1d, damage_siege, 1d, 1d, 1d));
        unit_catapult = constructibleRepository.save(unit_catapult);

        createConstructibleData(unit_catapult, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_catapult, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_catapult, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_catapult, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_catapult, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_catapult, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createRam() {
        unit_ram = createConstructible("ram", ConstructibleType.UNIT, 0);
        unit_ram.setUnitData(createUnitData(1d, armor_siege, 1d, damage_siege, 1d, 1d, 1d));
        unit_ram = constructibleRepository.save(unit_ram);

        createConstructibleData(unit_ram, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_ram, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_ram, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_ram, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_ram, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_ram, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createHeavyCavalry() {
        unit_heave_cavalry = createConstructible("heavy_cavalry", ConstructibleType.UNIT, 0);
        unit_heave_cavalry.setUnitData(createUnitData(1d, armor_plate, 1d, damage_spear, 1d, 1d, 1d));
        unit_heave_cavalry = constructibleRepository.save(unit_heave_cavalry);

        createConstructibleData(unit_heave_cavalry, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_heave_cavalry, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_heave_cavalry, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_heave_cavalry, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_heave_cavalry, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_heave_cavalry, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createLightCavalry() {
        unit_light_cavalry = createConstructible("light_cavalry", ConstructibleType.UNIT, 0);
        unit_light_cavalry.setUnitData(createUnitData(1d, armor_leather, 1d, damage_sword, 1d, 1d, 1d));
        unit_light_cavalry = constructibleRepository.save(unit_light_cavalry);

        createConstructibleData(unit_light_cavalry, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_light_cavalry, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_light_cavalry, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_light_cavalry, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_light_cavalry, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_light_cavalry, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createScout() {
        unit_scout = createConstructible("scout", ConstructibleType.UNIT, 0);
        unit_scout.setUnitData(createUnitData(1d, armor_fabric, 1d, damage_sword, 1d, 1d, 1d));
        unit_scout = constructibleRepository.save(unit_scout);

        createConstructibleData(unit_scout, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_scout, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_scout, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_scout, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_scout, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_scout, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createKnight() {
        unit_knight = createConstructible("knight", ConstructibleType.UNIT, 0);
        unit_knight.setUnitData(createUnitData(1d, armor_plate, 1d, damage_sword, 1d, 1d, 1d));
        unit_knight = constructibleRepository.save(unit_knight);

        createConstructibleData(unit_knight, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_knight, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_knight, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_knight, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_knight, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_knight, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createSpearman() {
        unit_spearman = createConstructible("spearman", ConstructibleType.UNIT, 0);
        unit_spearman.setUnitData(createUnitData(1d, armor_leather, 1d, damage_spear, 1d, 1d, 1d));
        unit_spearman = constructibleRepository.save(unit_spearman);

        createConstructibleData(unit_spearman, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_spearman, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_spearman, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_spearman, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_spearman, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_spearman, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createAxeman() {
        unit_axeman = createConstructible("axeman", ConstructibleType.UNIT, 0);
        unit_axeman.setUnitData(createUnitData(1d, armor_chain, 1d, damage_axe, 1d, 1d, 1d));
        unit_axeman = constructibleRepository.save(unit_axeman);

        createConstructibleData(unit_axeman, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_axeman, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_axeman, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_axeman, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_axeman, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_axeman, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createSwordsman() {
        unit_swordsman = createConstructible("swordsman", ConstructibleType.UNIT, 0);
        unit_swordsman.setUnitData(createUnitData(1d, armor_leather, 1d, damage_sword, 1d, 1d, 1d));
        unit_swordsman = constructibleRepository.save(unit_swordsman);

        createConstructibleData(unit_swordsman, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_swordsman, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_swordsman, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_swordsman, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_swordsman, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_swordsman, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createCrossbowman() {
        unit_crossbowman = createConstructible("crossbowman", ConstructibleType.UNIT, 0);
        unit_crossbowman.setUnitData(createUnitData(1d, armor_leather, 1d, damage_bolt, 1d, 1d, 1d));
        unit_crossbowman = constructibleRepository.save(unit_crossbowman);

        createConstructibleData(unit_crossbowman, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_crossbowman, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_crossbowman, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_crossbowman, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_crossbowman, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_crossbowman, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private void createArcher() {
        unit_archer = createConstructible("archer", ConstructibleType.UNIT, 0);
        unit_archer.setUnitData(createUnitData(1d, armor_fabric, 1d, damage_arrow, 1d, 1d, 1d));
        unit_archer = constructibleRepository.save(unit_archer);

        createConstructibleData(unit_archer, ConstructibleDatum.COST_WOOD, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_archer, ConstructibleDatum.COST_STONE, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_archer, ConstructibleDatum.COST_COAL, 10.0, 1.0, GrowthType.LINEAR);
        createConstructibleData(unit_archer, ConstructibleDatum.COST_IRON, 10.0, 1.0, GrowthType.LINEAR);

        createConstructibleData(unit_archer, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(unit_archer, ConstructibleDatum.CONSUMES_INHABITANTS, 10.0, 1.0, GrowthType.LINEAR);
    }

    private UnitData createUnitData(Double armor, ArmorType armorType,
                                    Double damage, DamageType damageType,
                                    Double health, Double speed, Double cargo) {
        UnitData data = new UnitData();
        data.setArmor(armor);
        data.setArmorType(armorType);
        data.setDamage(damage);
        data.setDamageType(damageType);
        data.setSpeed(speed);
        data.setCargo(cargo);
        data.setHealth(health);

        logger.debug(String.format("UnitData: %f %s, %f %s", armor, armorType.getName(), damage, damageType.getName()));

        return unitDataRepository.save(data);
    }

    private void createResearches() {
        createBowAndArrow();
        createCrossbows();
        createSwords();
        createAxes();
        createSpears();
        createLeatherArmor();
        createPlateArmor();
        createChainArmor();
        createSiegeWeapons();
        createCropRotation();
    }

    private void createCropRotation() {
        research_crop_rotation = createConstructible("crop_rotation", ConstructibleType.RESEARCH, 0);
        createConstructibleData(research_crop_rotation, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_crop_rotation, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_crop_rotation, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_crop_rotation, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_crop_rotation, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createSiegeWeapons() {
        research_siege_weapons = createConstructible("siege_weapons", ConstructibleType.RESEARCH, 0);
        createConstructibleData(research_siege_weapons, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_siege_weapons, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_siege_weapons, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_siege_weapons, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_siege_weapons, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createChainArmor() {
        research_chain_armor = createConstructible("chain_armor ", ConstructibleType.RESEARCH, 0);
        createConstructibleData(research_chain_armor, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_chain_armor, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_chain_armor, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_chain_armor, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_chain_armor, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createPlateArmor() {
        research_plate_armor = createConstructible("plate_armor", ConstructibleType.RESEARCH, 0);

        createConstructibleData(research_plate_armor, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_plate_armor, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_plate_armor, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_plate_armor, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_plate_armor, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createLeatherArmor() {
        research_leather_armor = createConstructible("leather_armor", ConstructibleType.RESEARCH, 0);

        createConstructibleData(research_leather_armor, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_leather_armor, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_leather_armor, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_leather_armor, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_leather_armor, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createSpears() {
        research_spears = createConstructible("spears", ConstructibleType.RESEARCH, 0);

        createConstructibleData(research_spears, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_spears, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_spears, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_spears, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_spears, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createAxes() {
        research_axes = createConstructible("axes", ConstructibleType.RESEARCH, 0);

        createConstructibleData(research_axes, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_axes, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_axes, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_axes, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_axes, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createSwords() {
        research_swords = createConstructible("swords", ConstructibleType.RESEARCH, 0);

        createConstructibleData(research_swords, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_swords, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_swords, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_swords, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_swords, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createCrossbows() {
        research_crossbows = createConstructible("crossbows", ConstructibleType.RESEARCH, 0);

        createConstructibleData(research_crossbows, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_crossbows, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_crossbows, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_crossbows, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_crossbows, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createBowAndArrow() {
        research_bow_and_arrow = createConstructible("bow_and_arrow", ConstructibleType.RESEARCH, 0);

        createConstructibleData(research_bow_and_arrow, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_bow_and_arrow, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_bow_and_arrow, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(research_bow_and_arrow, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(research_bow_and_arrow, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createBuildings() {
        createLumberjack();
        createQuarry();
        createCharcoalburner();
        createMine();
        createFarm();
        createHousing();
        createMarket();
        createStable();
        createBlacksmith();
        createBarracks();
        createDefenses();
        createStorage();
    }

    private void createStorage() {
        building_storage = createConstructible("storage", ConstructibleType.BUILDING, 11);

        createConstructibleData(building_storage, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_storage, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_storage, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_storage, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_storage, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_storage, ConstructibleDatum.STORAGE, 1000.0, 1.1, GrowthType.EXPONENTIAL);
    }

    private void createDefenses() {
        building_defenses = createConstructible("defenses", ConstructibleType.BUILDING, 10);

        createConstructibleData(building_defenses, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_defenses, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_defenses, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_defenses, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_defenses, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createBarracks() {
        building_barracks = createConstructible("barracks", ConstructibleType.BUILDING, 9);

        createConstructibleData(building_barracks, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_barracks, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_barracks, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_barracks, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_barracks, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createBlacksmith() {
        building_blacksmith = createConstructible("blacksmith", ConstructibleType.BUILDING, 8);

        createConstructibleData(building_blacksmith, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_blacksmith, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_blacksmith, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_blacksmith, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_blacksmith, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createStable() {
        building_stable = createConstructible("stable", ConstructibleType.BUILDING, 7);

        createConstructibleData(building_stable, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_stable, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_stable, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_stable, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_stable, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createMarket() {
        building_market = createConstructible("market", ConstructibleType.BUILDING, 6);

        createConstructibleData(building_market, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_market, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_market, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_market, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_market, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_market, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createHousing() {
        building_housing = createConstructible("housing", ConstructibleType.BUILDING, 5);

        createConstructibleData(building_housing, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_housing, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_housing, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_housing, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_housing, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_housing, ConstructibleDatum.LIVING_SPACE, 5.0, 1.0, GrowthType.LINEAR);
    }

    private void createFarm() {
        building_farm = createConstructible("farm", ConstructibleType.BUILDING, 1);

        createConstructibleData(building_farm, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_farm, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_farm, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_farm, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_farm, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_farm, ConstructibleDatum.PROD_FOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createMine() {
        building_mine = createConstructible("mine", ConstructibleType.BUILDING, 4);

        createConstructibleData(building_mine, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_mine, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_mine, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_mine, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_mine, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_mine, ConstructibleDatum.PROD_IRON, 1.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createCharcoalburner() {
        building_charcoalburner = createConstructible("charcoalburner", ConstructibleType.BUILDING, 3);

        createConstructibleData(building_charcoalburner, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_charcoalburner, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_charcoalburner, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_charcoalburner, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_charcoalburner, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_charcoalburner, ConstructibleDatum.PROD_COAL, 1.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createQuarry() {
        building_quarry = createConstructible("quarry", ConstructibleType.BUILDING, 2);

        createConstructibleData(building_quarry, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_quarry, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_quarry, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_quarry, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_quarry, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_quarry, ConstructibleDatum.PROD_STONE, 1.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createLumberjack() {
        building_lumberjack = createConstructible("lumberjack", ConstructibleType.BUILDING, 0);

        createConstructibleData(building_lumberjack, ConstructibleDatum.COST_WOOD, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_lumberjack, ConstructibleDatum.COST_STONE, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_lumberjack, ConstructibleDatum.COST_COAL, 5.0, 1.05, GrowthType.EXPONENTIAL);
        createConstructibleData(building_lumberjack, ConstructibleDatum.COST_IRON, 5.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_lumberjack, ConstructibleDatum.CONSTRUCTION_TICK_COUNT, 2.0, 1.05, GrowthType.EXPONENTIAL);

        createConstructibleData(building_lumberjack, ConstructibleDatum.PROD_WOOD, 1.0, 1.05, GrowthType.EXPONENTIAL);
    }

    private void createConstructibleData(Constructible constructible, String resource, Double amount, Double growth, GrowthType growthType) {
        ConstructibleDatum cost = new ConstructibleDatum();
        cost.setConstructible(constructible);
        cost.setName(resource);
        cost.setValue(amount);
        cost.setGrowth(growth);
        cost.setGrowthType(growthType);

        logger.debug(constructible.getId() + " added const " + resource + ": " + amount);

        constructibleDataRepository.save(cost);
    }

    private Constructible createConstructible(String name, ConstructibleType type, int sortOrder) {
        Constructible c = new Constructible();
        c.setId(name);
        c.setDescription("description");
        c.setType(type);
        c.setSorting(sortOrder);

        logger.debug("Created " + type.toString() + ": " + name);

        return constructibleRepository.save(c);
    }

    private void createDamageAndArmor() {
        armor_leather = createArmorType("leather");
        armor_plate = createArmorType("plate");
        armor_chain = createArmorType("chain");
        armor_fabric = createArmorType("fabric");
        armor_siege = createArmorType("siege");

        damage_blunt = createDamageType("blunt");
        damage_arrow = createDamageType("arrow");
        damage_bolt = createDamageType("bolt");
        damage_sword = createDamageType("sword");
        damage_spear = createDamageType("spear");
        damage_axe = createDamageType("axe");
        damage_siege = createDamageType("siege");
    }

    private DamageType createDamageType(String name) {
        DamageType type = new DamageType();
        type.setName(name);

        logger.debug("Create damage type " + name);

        return damageTypeRepository.save(type);
    }

    private ArmorType createArmorType(String name) {
        ArmorType armorType = new ArmorType();
        armorType.setName(name);

        logger.debug("Created armor type " + name);

        return armorTypeRepository.save(armorType);
    }

    private void createResources() {
        resource_coal = createResource("coal", true);
        resource_iron = createResource("iron", true);
        resource_stone = createResource("stone", true);
        resource_wood = createResource("wood", true);

        resource_food = createResource("food", true);
    }

    private Resource createResource(String name, boolean tradeable) {
        Resource res = new Resource();
        res.setName(name);
        res.setTradeable(tradeable);

        logger.debug("Created resource " + name);

        return resourceRepository.save(res);
    }

    private void setupPlayer() {
        admin = createPlayer("admin", "mail@admin.admin", "password");
        createRoleForPlayer(admin, Role.user());
        createRoleForPlayer(admin, Role.admin());
        admin = playerRepository.save(admin);
        createVillageForPlayer(admin, "Admin Village", 0L, 0L, true);

        player = createPlayer("player", "player@player.player", "password");
        createRoleForPlayer(player, Role.user());
        player = playerRepository.save(player);
        createVillageForPlayer(player, "Player Village", 1L, 0L, true);
    }

    private void createRoleForPlayer(Player player, Role role) {
        role.setPlayer(player);
        player.getRoles().add(role);
    }

    private void createVillageForPlayer(Player player, String villageName, Long x, Long y, Boolean focused) {
        Village village = new Village();
        village.setName(villageName);
        village.setX(x);
        village.setY(y);
        village.setFocused(focused);
        village.setPlayer(player);

        village = villageRepository.save(village);

        createResourceInVillage(resource_wood, village, 100.0);
        createResourceInVillage(resource_stone, village, 100.0);
        createResourceInVillage(resource_iron, village, 100.0);
        createResourceInVillage(resource_coal, village, 100.0);

        createResourceInVillage(resource_food, village, 0.0);

        logger.debug("Created Village " + villageName + " for player " + player.getName());
    }

    private void createResourceInVillage(Resource resource, Village village, Double amount) {
        VillageHasResource villageHasResource = new VillageHasResource();
        villageHasResource.setResource(resource);
        villageHasResource.setAmount(amount);
        villageHasResource.setVillage(village);

        logger.debug("Added " + amount + " " + resource.getName() + " to Village " + village.getName());

        villageHasResourceRepository.save(villageHasResource);
    }

    private Player createPlayer(String name, String email, String password) {
        Player player = new Player();
        player.setName(name);
        player.setEmail(email);
        player.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));

        logger.debug("Created player " + name);

        return playerRepository.save(player);
    }
}

package de.almostintelligent.tickgame.fixture;

import de.almostintelligent.tickgame.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class FixturesStartupListener implements ApplicationListener<EmbeddedServletContainerInitializedEvent> {

    Logger logger = LoggerFactory.getLogger("de.almostintelligent.tickgame");

    private PlayerRepository playerRepository;
    private ResourceRepository resourceRepository;
    private DamageTypeRepository damageTypeRepository;
    private ArmorTypeRepository armorTypeRepository;
    private ConstructibleRepository constructibleRepository;
    private ConstructibleDataRepository constructibleCostsResourceRepository;
    private UnitDataRepository unitDataRepository;
    private ConstructibleHasDependencyRepository constructibleHasDependencyRepository;
    private ConstructibleDataRepository constructibleDataRepository;
    private VillageRepository villageRepository;
    private VillageHasResourceRepository villageHasResourceRepository;
    private BlogRepository blogRepository;
    private RoleRepository roleRepository;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernateHBM2ddlAuto;

    @Autowired
    public FixturesStartupListener(PlayerRepository playerRepository, ResourceRepository resourceRepository, DamageTypeRepository damageTypeRepository, ArmorTypeRepository armorTypeRepository, ConstructibleRepository constructibleRepository, ConstructibleDataRepository constructibleCostsResourceRepository, UnitDataRepository unitDataRepository, ConstructibleHasDependencyRepository constructibleHasDependencyRepository, ConstructibleDataRepository constructibleDataRepository, VillageRepository villageRepository, VillageHasResourceRepository villageHasResourceRepository, BlogRepository blogRepository, RoleRepository roleRepository) {
        this.playerRepository = playerRepository;
        this.resourceRepository = resourceRepository;
        this.damageTypeRepository = damageTypeRepository;
        this.armorTypeRepository = armorTypeRepository;
        this.constructibleRepository = constructibleRepository;
        this.constructibleCostsResourceRepository = constructibleCostsResourceRepository;
        this.unitDataRepository = unitDataRepository;
        this.constructibleHasDependencyRepository = constructibleHasDependencyRepository;
        this.constructibleDataRepository = constructibleDataRepository;
        this.villageRepository = villageRepository;
        this.villageHasResourceRepository = villageHasResourceRepository;
        this.blogRepository = blogRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {
        if (hibernateHBM2ddlAuto.equalsIgnoreCase("create")) {
            FixturesManager manager = new FixturesManager(
                    playerRepository,
                    resourceRepository,
                    damageTypeRepository,
                    armorTypeRepository,
                    constructibleRepository,
                    unitDataRepository,
                    constructibleHasDependencyRepository,
                    villageRepository,
                    villageHasResourceRepository,
                    constructibleDataRepository, blogRepository, roleRepository);
            manager.run();
        }
    }
}

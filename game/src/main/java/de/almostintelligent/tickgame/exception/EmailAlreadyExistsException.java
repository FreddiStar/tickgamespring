package de.almostintelligent.tickgame.exception;

public class EmailAlreadyExistsException extends Exception {
    public EmailAlreadyExistsException() {
        super("E-Mail already taken");
    }
}

package de.almostintelligent.tickgame.exception;

public class PasswordsDoNotMatchException extends Exception {
    public PasswordsDoNotMatchException() {
        super("Passwords don't match");
    }
}

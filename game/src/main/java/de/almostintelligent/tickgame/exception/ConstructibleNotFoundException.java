package de.almostintelligent.tickgame.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FAILED_DEPENDENCY)
public class ConstructibleNotFoundException extends Exception {
    public ConstructibleNotFoundException() {
        super("Constructible not found");
    }
}

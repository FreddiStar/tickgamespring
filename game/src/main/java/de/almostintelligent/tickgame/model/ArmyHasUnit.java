package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name = "army_has_unit")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ArmyHasUnit extends BaseEntity {

    private Double x;
    private Double y;
    private Double angle;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "army_id")
    private Army army;

    @OneToOne
    private PlayerHasUnit playerHasUnit;
}

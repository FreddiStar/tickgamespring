package de.almostintelligent.tickgame.model.types;


import com.fasterxml.jackson.annotation.JsonCreator;

public enum ConstructibleType {
    BUILDING, UNIT, RESEARCH;

    @JsonCreator
    public static ConstructibleType forValue(String value) {
        return valueOf(value.toUpperCase());
    }
}

package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity(name = "player_deposit")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PlayerDeposit extends BaseEntity {

    private Long amount;

    @OneToOne
    private Player player;

}

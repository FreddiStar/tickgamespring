package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name = "player_has_unit")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PlayerHasUnit extends BaseEntity {

    @OneToOne(mappedBy = "playerHasUnit")
    protected ArmyHasUnit armyHasUnit;

    @OneToOne(mappedBy = "playerHasUnit")
    protected VillageHasUnit villageHasUnit;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "player_id")
    protected Player player;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "constructible_id")
    protected Constructible constructible;

    public ArmyHasUnit getArmyHasUnit() {
        return armyHasUnit;
    }

    public void setArmyHasUnit(ArmyHasUnit armyHasUnit) {
        this.armyHasUnit = armyHasUnit;
    }

    public VillageHasUnit getVillageHasUnit() {
        return villageHasUnit;
    }

    public void setVillageHasUnit(VillageHasUnit villageHasUnit) {
        this.villageHasUnit = villageHasUnit;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Constructible getConstructible() {
        return constructible;
    }

    public void setConstructible(Constructible constructible) {
        this.constructible = constructible;
    }
}

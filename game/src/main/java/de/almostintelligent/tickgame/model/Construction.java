package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity(name = "construction")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Construction extends BaseEntity {

    @Column(name = "ticks_left")
    protected Long ticksLeft;
    protected Long amount;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "constructible_id")
    protected Constructible constructible;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "village_id")
    protected Village village;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "player_id")
    protected Player player;

    public Long getTicksLeft() {
        return ticksLeft;
    }

    public void setTicksLeft(Long ticksLeft) {
        this.ticksLeft = ticksLeft;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Constructible getConstructible() {
        return constructible;
    }

    public void setConstructible(Constructible constructible) {
        this.constructible = constructible;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Village getVillage() {
        return village;
    }

    public void setVillage(Village village) {
        this.village = village;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}

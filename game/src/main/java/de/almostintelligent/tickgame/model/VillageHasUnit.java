package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.almostintelligent.tickgame.constructible.ConstructibleDependencyCompliance;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "village_has_unit")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class VillageHasUnit extends BaseEntity implements ConstructibleDependencyCompliance {

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "village_id")
    protected Village village;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "player_has_unit_id")
    protected PlayerHasUnit playerHasUnit;

    @Override
    public Long getLevel() {
        return 0L;
    }

    @Override
    public Constructible getConstructible() {
        return playerHasUnit.constructible;
    }

    public Village getVillage() {
        return village;
    }

    public void setVillage(Village village) {
        this.village = village;
    }

    public PlayerHasUnit getPlayerHasUnit() {
        return playerHasUnit;
    }

    public void setPlayerHasUnit(PlayerHasUnit playerHasUnit) {
        this.playerHasUnit = playerHasUnit;
    }
}

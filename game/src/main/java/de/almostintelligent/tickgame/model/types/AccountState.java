package de.almostintelligent.tickgame.model.types;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum AccountState {
    PENDING, CONFIRMED, DISABLED;

    @JsonCreator
    public static AccountState forValue(String value) {
        return valueOf(value.toUpperCase());
    }
}


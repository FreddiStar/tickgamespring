package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.almostintelligent.tickgame.model.types.ScoreType;

import javax.persistence.*;

@Entity(name = "score")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Score extends BaseEntity {

    @ManyToOne(optional = false)
    @JoinColumn(referencedColumnName = "id", name = "player_id")
    private Player player;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ScoreType type;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "village_id")
    private Village village;

    @Column(nullable = false)
    private Double score;

    @JsonIdentityReference(alwaysAsId = true)
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ScoreType getType() {
        return type;
    }

    public void setType(ScoreType type) {
        this.type = type;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Village getVillage() {
        return village;
    }

    public void setVillage(Village village) {
        this.village = village;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}

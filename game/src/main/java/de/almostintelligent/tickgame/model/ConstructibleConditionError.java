package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

public class ConstructibleConditionError {
    private String id;
    private String message;
    private Constructible constructible;

    public ConstructibleConditionError(String id, String message, Constructible constructible) {
        this.id = id + "_" + constructible.getId();
        this.message = message;
        this.constructible = constructible;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Constructible getConstructible() {
        return constructible;
    }

    public void setConstructible(Constructible constructible) {
        this.constructible = constructible;
    }
}

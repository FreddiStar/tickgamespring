package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.almostintelligent.tickgame.model.BaseEntity;
import de.almostintelligent.tickgame.model.DamageVsArmor;
import de.almostintelligent.tickgame.model.UnitData;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "damage_type")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class DamageType extends BaseEntity {

    protected String name;

    @OneToMany(mappedBy = "damageType")
    protected List<DamageVsArmor> damageVsArmors;

    @OneToMany(mappedBy = "damageType")
    protected List<UnitData> unitDatas;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DamageVsArmor> getDamageVsArmors() {
        return damageVsArmors;
    }

    public void setDamageVsArmors(List<DamageVsArmor> damageVsArmors) {
        this.damageVsArmors = damageVsArmors;
    }

    public List<UnitData> getUnitDatas() {
        return unitDatas;
    }

    public void setUnitDatas(List<UnitData> unitDatas) {
        this.unitDatas = unitDatas;
    }
}

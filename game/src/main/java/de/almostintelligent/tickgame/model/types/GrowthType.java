package de.almostintelligent.tickgame.model.types;

public enum GrowthType {
    LINEAR, EXPONENTIAL
}

package de.almostintelligent.tickgame.model.types;

public enum ScoreType {
    BUILDING, RESEARCH, UNIT
}

package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(name = "village")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Village extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Long x;

    @Column(nullable = false)
    private Long y;

    @Column(nullable = false)
    private Boolean focused;

    @OneToMany(mappedBy = "village", fetch = FetchType.LAZY)
    private List<Construction> constructions;

    @OneToMany(mappedBy = "village", fetch = FetchType.LAZY)
    private List<VillageHasBuilding> villageHasBuildings;

    @OneToMany(mappedBy = "village", fetch = FetchType.LAZY)
    private List<VillageHasResource> villageHasResources;

    @OneToMany(mappedBy = "village", fetch = FetchType.LAZY)
    private List<VillageHasUnit> villageHasUnits;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "player_id")
    private Player player;

    @OneToMany(mappedBy = "village")
    private List<Score> villageScores;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getX() {
        return x;
    }

    public void setX(Long x) {
        this.x = x;
    }

    public Long getY() {
        return y;
    }

    public void setY(Long y) {
        this.y = y;
    }

    public Boolean getFocused() {
        return focused;
    }

    public void setFocused(Boolean focused) {
        this.focused = focused;
    }

    @JsonIgnore
    public List<Construction> getConstructions() {
        return constructions;
    }

    public void setConstructions(List<Construction> constructions) {
        this.constructions = constructions;
    }

    @JsonIgnore
    public List<VillageHasBuilding> getVillageHasBuildings() {
        return villageHasBuildings;
    }

    public void setVillageHasBuildings(List<VillageHasBuilding> villageHasBuildings) {
        this.villageHasBuildings = villageHasBuildings;
    }

    @JsonIgnore
    public List<VillageHasResource> getVillageHasResources() {
        return villageHasResources;
    }

    public void setVillageHasResources(List<VillageHasResource> villageHasResources) {
        this.villageHasResources = villageHasResources;
    }

    @JsonIgnore
    public List<VillageHasUnit> getVillageHasUnits() {
        return villageHasUnits;
    }

    public void setVillageHasUnits(List<VillageHasUnit> villageHasUnits) {
        this.villageHasUnits = villageHasUnits;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<Score> getVillageScores() {
        return villageScores;
    }

    public void setVillageScores(List<Score> villageScores) {
        this.villageScores = villageScores;
    }

    @JsonIgnore
    public Map<String, VillageHasBuilding> getVillageHasBuildingsMap() {
        HashMap<String, VillageHasBuilding> result = new HashMap<String, VillageHasBuilding>();

        for (VillageHasBuilding villageHasBuilding : getVillageHasBuildings()) {
            result.put(villageHasBuilding.getConstructible().getId(), villageHasBuilding);
        }
        return result;
    }

    @JsonIgnore
    public Map<String, VillageHasUnit> getVillageHasUnitsMap() {
        HashMap<String, VillageHasUnit> result = new HashMap<String, VillageHasUnit>();

        for (VillageHasUnit villageHasUnit : getVillageHasUnits()) {
            result.put(villageHasUnit.getConstructible().getId(), villageHasUnit);
        }
        return result;
    }
}

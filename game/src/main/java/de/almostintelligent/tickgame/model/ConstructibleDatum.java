package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.almostintelligent.tickgame.model.types.GrowthType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "constructible_datum")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ConstructibleDatum {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String id;

    private String name;
    private Double value;
    private Double growth;

    @Enumerated(EnumType.STRING)
    private GrowthType growthType;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "constructible_id")
    private Constructible constructible;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getGrowth() {
        return growth;
    }

    public void setGrowth(Double growth) {
        this.growth = growth;
    }

    public GrowthType getGrowthType() {
        return growthType;
    }

    public void setGrowthType(GrowthType growthType) {
        this.growthType = growthType;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Constructible getConstructible() {
        return constructible;
    }

    public void setConstructible(Constructible constructible) {
        this.constructible = constructible;
    }

    public Double grow(Long level) {
        level -= 1;
        if (getGrowthType().equals(GrowthType.EXPONENTIAL)) {
            return value * Math.pow(1 + growth, level.doubleValue());
        } else {
            return growth * level + value;
        }
    }

    // Resources
    public static final String COST_WOOD = "COST_WOOD";
    public static final String COST_IRON = "COST_IRON";
    public static final String COST_STONE = "COST_STONE";
    public static final String COST_COAL = "COST_COAL";

    public static final String CONSUMES_INHABITANTS = "CONSUMES_INHABITANTS";

    public static final String PROD_WOOD = "PROD_WOOD";
    public static final String PROD_IRON = "PROD_IRON";
    public static final String PROD_STONE = "PROD_STONE";
    public static final String PROD_COAL = "PROD_COAL";

    public static final String PROD_FOOD = "PROD_FOOD";

    public static final String CONSTRUCTION_TICK_COUNT = "CONSTRUCTION_TICK_COUNT";

    // Village
    public static final String STORAGE = "STORAGE";

    // Inhabitants
    public static final String INHABITANTS = "INHABITANTS";
    public static final String LIVING_SPACE = "LIVING_SPACE";
    public static final String NEEDS_WORKER = "NEEDS_WORKER";
}

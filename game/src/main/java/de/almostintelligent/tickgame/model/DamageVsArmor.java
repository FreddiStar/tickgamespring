package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "damage_vs_armor")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class DamageVsArmor extends BaseEntity {

    protected Float factor;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "damage_type_id")
    protected DamageType damageType;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "armor_type_id")
    protected ArmorType armorType;
}

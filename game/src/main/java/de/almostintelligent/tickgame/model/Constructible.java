package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.almostintelligent.tickgame.model.types.ConstructibleType;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(name = "constructible")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Constructible {

    @Id
    public String id;

    @Column(nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ConstructibleType type;

    @Column(nullable = false)
    private Integer sorting;

    @OneToMany(mappedBy = "constructible")
    @MapKey(name = "name")
    private Map<String, ConstructibleDatum> constructibleDatas = new HashMap<String, ConstructibleDatum>();

    @OneToMany(mappedBy = "constructible")
    private List<ConstructibleHasDependency> constructibleHasDependencies;

    @OneToOne
    @JoinColumn(referencedColumnName = "id", name = "unit_data_id")
    private UnitData unitData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setConstructibleDatas(Map<String, ConstructibleDatum> constructibleDatas) {
        this.constructibleDatas = constructibleDatas;
    }

    public void setConstructibleHasDependencies(List<ConstructibleHasDependency> constructibleHasDependencies) {
        this.constructibleHasDependencies = constructibleHasDependencies;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSorting() {
        return sorting;
    }

    public void setSorting(Integer sorting) {
        this.sorting = sorting;
    }

    public ConstructibleType getType() {
        return type;
    }

    public void setType(ConstructibleType type) {
        this.type = type;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public UnitData getUnitData() {
        return unitData;
    }

    public void setUnitData(UnitData unitData) {
        this.unitData = unitData;
    }

    @JsonIgnore
    public List<ConstructibleHasDependency> getConstructibleHasDependencies() {
        return constructibleHasDependencies;
    }

    @JsonIgnore
    public Map<String, ConstructibleDatum> getConstructibleDatas() {
        return constructibleDatas;
    }

}

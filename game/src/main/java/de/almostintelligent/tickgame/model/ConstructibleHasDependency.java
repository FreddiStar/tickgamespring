package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "constructible_has_dependency")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ConstructibleHasDependency extends BaseEntity {

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "constructible_id")
    private Constructible constructible;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "dependency_id")
    private Constructible dependency;

    @Column(nullable = false)
    private Integer level;

    @JsonIdentityReference(alwaysAsId = true)
    public Constructible getConstructible() {
        return constructible;
    }

    public void setConstructible(Constructible constructible) {
        this.constructible = constructible;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Constructible getDependency() {
        return dependency;
    }

    public void setDependency(Constructible dependency) {
        this.dependency = dependency;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}

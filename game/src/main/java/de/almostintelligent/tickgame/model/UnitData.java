package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity(name = "unit_data")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UnitData extends BaseEntity {

    @Column(nullable = false)
    protected Double damage;
    @Column(nullable = false)
    protected Double armor;
    @Column(nullable = false)
    protected Double health;
    @Column(nullable = false)
    protected Double cargo;
    @Column(nullable = false)
    protected Double speed;

    @OneToOne(mappedBy = "unitData")
    protected Constructible constructible;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "armor_type_id", nullable = false)
    protected ArmorType armorType;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "damage_type_id", nullable = false)
    protected DamageType damageType;

    public Double getDamage() {
        return damage;
    }

    public void setDamage(Double damage) {
        this.damage = damage;
    }

    public Double getArmor() {
        return armor;
    }

    public void setArmor(Double armor) {
        this.armor = armor;
    }

    public Double getHealth() {
        return health;
    }

    public void setHealth(Double health) {
        this.health = health;
    }

    public Double getCargo() {
        return cargo;
    }

    public void setCargo(Double cargo) {
        this.cargo = cargo;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Constructible getConstructible() {
        return constructible;
    }

    public void setConstructible(Constructible constructible) {
        this.constructible = constructible;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public DamageType getDamageType() {
        return damageType;
    }

    public void setDamageType(DamageType damageType) {
        this.damageType = damageType;
    }
}

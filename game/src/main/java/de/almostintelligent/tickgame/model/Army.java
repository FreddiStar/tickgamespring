package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "army")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Army extends BaseEntity {

    private String name;
    private Integer x;
    private Integer y;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "player_id")
    private Player player;

    @OneToMany(mappedBy = "army")
    private List<ArmyHasUnit> armyHasUnits;

}

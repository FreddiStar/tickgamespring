package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.almostintelligent.tickgame.model.types.AccountState;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity(name = "player")
@EqualsAndHashCode(callSuper = false)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Player extends BaseEntity implements UserDetails {

    private String name;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    protected Date lastLogin;

    @ManyToOne
    @JoinColumn(name = "alliance_id", referencedColumnName = "id")
    protected Alliance alliance;

    @OneToMany(mappedBy = "player")
    protected List<Army> armies;

    @OneToMany(mappedBy = "player")
    protected List<Construction> constructions;

    @OneToOne
    protected PlayerDeposit playerDeposit;

    @OneToMany(mappedBy = "player")
    protected List<PlayerHasResearch> playerHasResearches;

    @OneToMany(mappedBy = "player")
    protected List<PlayerHasUnit> playerHasUnits;

    @OneToMany(mappedBy = "player")
    protected List<Village> villages;

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    protected Set<Role> roles = new HashSet<Role>();

    @Enumerated(EnumType.STRING)
    protected AccountState accountState = AccountState.PENDING;

    @OneToMany(mappedBy = "player")
    private List<Score> scores;

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @JsonIgnore
    public Map<String, PlayerHasResearch> getPlayerHasResearchesMap() {
        HashMap<String, PlayerHasResearch> result = new HashMap<String, PlayerHasResearch>();
        for (PlayerHasResearch playerHasResearch : getPlayerHasResearches()) {
            result.put(playerHasResearch.getConstructible().getId(), playerHasResearch);
        }

        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Alliance getAlliance() {
        return alliance;
    }

    public void setAlliance(Alliance alliance) {
        this.alliance = alliance;
    }

    @JsonIgnore
    public List<Army> getArmies() {
        return armies;
    }

    public void setArmies(List<Army> armies) {
        this.armies = armies;
    }

    @JsonIgnore
    public List<Construction> getConstructions() {
        return constructions;
    }

    public void setConstructions(List<Construction> constructions) {
        this.constructions = constructions;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public PlayerDeposit getPlayerDeposit() {
        return playerDeposit;
    }

    public void setPlayerDeposit(PlayerDeposit playerDeposit) {
        this.playerDeposit = playerDeposit;
    }

    @JsonIgnore
    public List<PlayerHasResearch> getPlayerHasResearches() {
        return playerHasResearches;
    }

    public void setPlayerHasResearches(List<PlayerHasResearch> playerHasResearches) {
        this.playerHasResearches = playerHasResearches;
    }

    @JsonIgnore
    public List<PlayerHasUnit> getPlayerHasUnits() {
        return playerHasUnits;
    }

    public void setPlayerHasUnits(List<PlayerHasUnit> playerHasUnits) {
        this.playerHasUnits = playerHasUnits;
    }

    @JsonIgnore
    public List<Village> getVillages() {
        return villages;
    }

    public void setVillages(List<Village> villages) {
        this.villages = villages;
    }

    @JsonIgnore
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public AccountState getAccountState() {
        return accountState;
    }

    public void setAccountState(AccountState accountState) {
        this.accountState = accountState;
    }
}

package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.function.BooleanSupplier;

@Entity(name = "resource")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Resource extends BaseEntity {

    protected String name;
    protected Boolean tradeable;

    @OneToMany(mappedBy = "resource")
    protected List<VillageHasResource> villageHasResources;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<VillageHasResource> getVillageHasResources() {
        return villageHasResources;
    }

    public void setVillageHasResources(List<VillageHasResource> villageHasResources) {
        this.villageHasResources = villageHasResources;
    }

    public Boolean getTradeable() {
        return tradeable;
    }

    public void setTradeable(Boolean tradeable) {
        this.tradeable = tradeable;
    }
}

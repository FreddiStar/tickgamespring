package de.almostintelligent.tickgame.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.almostintelligent.tickgame.constructible.ConstructibleDependencyCompliance;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "village_has_building")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class VillageHasBuilding extends BaseEntity implements ConstructibleDependencyCompliance {

    private Long level;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "village_id")
    private Village village;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "constructible_id")
    private Constructible constructible;

    @JsonIdentityReference(alwaysAsId = true)
    public Village getVillage() {
        return village;
    }

    public void setVillage(Village village) {
        this.village = village;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Constructible getConstructible() {
        return constructible;
    }

    public void setConstructible(Constructible constructible) {
        this.constructible = constructible;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }
}

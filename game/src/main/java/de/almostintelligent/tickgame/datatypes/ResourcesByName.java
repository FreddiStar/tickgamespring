package de.almostintelligent.tickgame.datatypes;

import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.VillageHasResource;

import java.util.HashMap;

public class ResourcesByName extends HashMap<String, Double> {

    public static ResourcesByName fromVillage(Village village) {
        ResourcesByName result = new ResourcesByName();
        for (VillageHasResource resource : village.getVillageHasResources()) {
            result.put(resource.getResource().getName(), resource.getAmount());
        }

        return result;
    }

    public boolean hasEnought(ResourcesByName resources) {
        for (String name : resources.keySet()) {
            if (!hasEnought(name, resources.get(name))) {
                return false;
            }
        }

        return true;
    }

    public boolean hasEnought(String name, Double amount) {
        if (!containsKey(name) && amount > 0L) {
            return false;
        } else {
            return get(name) > amount;
        }
    }
}

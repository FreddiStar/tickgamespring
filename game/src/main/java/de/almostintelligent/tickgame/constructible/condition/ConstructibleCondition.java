package de.almostintelligent.tickgame.constructible.condition;

import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.types.ConstructibleType;

import java.util.Set;

public interface ConstructibleCondition {
    boolean meet(Constructible constructible, Long level, Village village, Player player);

    Set<ConstructibleType> type();

    String error();

    String id();
}

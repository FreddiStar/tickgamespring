package de.almostintelligent.tickgame.constructible;

import de.almostintelligent.tickgame.model.Constructible;

public interface ConstructibleDependencyCompliance {

    Long getLevel();

    Constructible getConstructible();

}

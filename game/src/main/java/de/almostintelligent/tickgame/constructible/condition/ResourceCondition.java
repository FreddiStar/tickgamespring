package de.almostintelligent.tickgame.constructible.condition;

import de.almostintelligent.tickgame.datatypes.ResourcesByName;
import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.service.ResourceService;
import de.almostintelligent.tickgame.service.VillageService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

@RegisterAsConstructibleCondition
public class ResourceCondition implements ConstructibleCondition {

    private ResourceService resourceService;
    private VillageService villageService;

    @Autowired
    public ResourceCondition(ResourceService resourceService,
                             VillageService villageService) {
        this.resourceService = resourceService;
        this.villageService = villageService;
    }

    @Override
    public boolean meet(Constructible constructible, Long level, Village village, Player player) {
        ResourcesByName cost = resourceService.getConstructibleCostForLevel(constructible, level);
        return villageService.villageHasResources(village, cost);
    }

    @Override
    public Set<ConstructibleType> type() {
        HashSet<ConstructibleType> result = new HashSet<ConstructibleType>();
        result.add(ConstructibleType.BUILDING);
        result.add(ConstructibleType.RESEARCH);
        result.add(ConstructibleType.UNIT);
        return result;
    }

    @Override
    public String error() {
        return "Nicht genug Rohstoffe";
    }

    @Override
    public String id() {
        return "resource_condition";
    }
}

package de.almostintelligent.tickgame.constructible.condition;

import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;

import java.util.HashSet;
import java.util.Set;

@RegisterAsConstructibleCondition
public class MaxLevelNotReachedCondition implements ConstructibleCondition {
    @Override
    public boolean meet(Constructible constructible, Long level, Village village, Player playerv) {
        return true;
    }

    @Override
    public Set<ConstructibleType> type() {
        HashSet<ConstructibleType> result = new HashSet<ConstructibleType>();
        result.add(ConstructibleType.BUILDING);
        result.add(ConstructibleType.RESEARCH);
        result.add(ConstructibleType.UNIT);
        return result;
    }

    @Override
    public String error() {
        return "Maximales Level erreicht";
    }

    @Override
    public String id() {
        return "max_level_reached_condition";
    }
}

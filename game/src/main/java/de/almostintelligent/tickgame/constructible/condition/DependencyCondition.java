package de.almostintelligent.tickgame.constructible.condition;

import de.almostintelligent.tickgame.constructible.ConstructibleDependencyCompliance;
import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.ConstructibleHasDependency;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.types.ConstructibleType;

import java.util.*;

@RegisterAsConstructibleCondition
public class DependencyCondition implements ConstructibleCondition {

    @Override
    public boolean meet(Constructible constructible, Long level, Village village, Player player) {
        List<ConstructibleHasDependency> dependencies = constructible.getConstructibleHasDependencies();

        Map<String, ConstructibleDependencyCompliance> constructibleDependencyCompliances = new HashMap<>();

        village.getVillageHasBuildings().stream().forEach(b -> constructibleDependencyCompliances.put(b.getId(), b));
        player.getPlayerHasResearches().stream().forEach(r -> constructibleDependencyCompliances.put(r.getId(), r));

        for (ConstructibleHasDependency dependency : dependencies) {
            String dependencyId = dependency.getConstructible().getId();

            ConstructibleDependencyCompliance compliance = constructibleDependencyCompliances.get(dependencyId);
            if (compliance == null) {
                return false;
            }

            if (compliance.getLevel() < dependency.getLevel()) {
                return false;
            }

        }

        return true;
    }

    @Override
    public Set<ConstructibleType> type() {
        HashSet<ConstructibleType> result = new HashSet<ConstructibleType>();
        result.add(ConstructibleType.BUILDING);
        result.add(ConstructibleType.RESEARCH);
        result.add(ConstructibleType.UNIT);
        return result;
    }

    @Override
    public String error() {
        return "Vorrausetzungen nicht erfüllt";
    }

    @Override
    public String id() {
        return "dependency_condition";
    }
}

package de.almostintelligent.tickgame.constructible;

public interface ConstructibleModifier {
    void modify();
}

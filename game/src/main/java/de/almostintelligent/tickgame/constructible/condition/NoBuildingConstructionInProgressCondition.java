package de.almostintelligent.tickgame.constructible.condition;

import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.Construction;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.types.ConstructibleType;

import java.util.HashSet;
import java.util.Set;

@RegisterAsConstructibleCondition
public class NoBuildingConstructionInProgressCondition implements ConstructibleCondition {
    @Override
    public boolean meet(Constructible constructible, Long level, Village village, Player player) {
        if (village.getConstructions().isEmpty()) {
            return true;
        } else {
            for (Construction construction : village.getConstructions()) {
                if (construction.getConstructible().getType().equals(ConstructibleType.BUILDING)) {
                    return false;
                }
            }

            return true;
        }
    }

    @Override
    public Set<ConstructibleType> type() {
        HashSet<ConstructibleType> result = new HashSet<ConstructibleType>();
        result.add(ConstructibleType.BUILDING);
        //result.add(ConstructibleType.RESEARCH);
        //result.add(ConstructibleType.UNIT);
        return result;
    }

    @Override
    public String error() {
        return "Es wird bereits gebaut";
    }

    @Override
    public String id() {
        return "no_building_construction_in_progress_condition";
    }
}

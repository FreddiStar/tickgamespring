package de.almostintelligent.tickgame.controller;

import de.almostintelligent.tickgame.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.logging.Logger;

@RestController
public class DashboardController {

    private Logger logger = Logger.getLogger(DashboardController.class.getSimpleName());
    private PlayerService playerAuthenticationService;

    @Autowired
    public DashboardController(PlayerService playerAuthenticationService) {
        this.playerAuthenticationService = playerAuthenticationService;
    }

    @RequestMapping(value = "/api/dashboard", method = RequestMethod.GET)
    public HashMap<String, Object> getDashboard() {

        System.err.println("getDashboard");
        logger.fine("getDashboard");

        //Player player = playerAuthenticationService.getLoggedInPlayer();

        return new HashMap<String, Object>() {{
            put("success", true);
        }};
    }

}

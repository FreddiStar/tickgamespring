package de.almostintelligent.tickgame.controller;

import de.almostintelligent.tickgame.api.dto.ResourceByNameDTO;
import de.almostintelligent.tickgame.api.response.VillageInfoResponse;
import de.almostintelligent.tickgame.exception.PlayerNotFoundException;
import de.almostintelligent.tickgame.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VillageController {

    private PlayerService playerService;

    @Autowired
    public VillageController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @RequestMapping("/api/village/info")
    public VillageInfoResponse getResourcesFromFocusedVillage() throws PlayerNotFoundException {
        VillageInfoResponse response = new VillageInfoResponse();
        response.setResources(ResourceByNameDTO.fromVillage(playerService.focusedVillage()));
        return response;
    }

}

package de.almostintelligent.tickgame.controller;

import de.almostintelligent.tickgame.ember.EmberModel;
import de.almostintelligent.tickgame.exception.PlayerNotFoundException;
import de.almostintelligent.tickgame.media.MediaType;
import de.almostintelligent.tickgame.model.Alliance;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Role;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class CurrentPlayerController {

    private PlayerService playerService;

    @Autowired
    public CurrentPlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @RequestMapping(
            value = "/api/currentPlayers ",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8)
    public EmberModel getCurrentPlayer() throws PlayerNotFoundException {
        Player player = playerService.player();
        Collection<Player> players = new ArrayList<Player>();
        players.add(player);
        EmberModel.Builder builder = new EmberModel.Builder("currentPlayer", players);
        builder.sideLoad(Role.class, player.getRoles());
        builder.sideLoad(Village.class, player.getVillages());
        if (player.getAlliance() != null) {
            builder.sideLoad(Alliance.class, player.getAlliance(), true);
        }
        return builder.build();
    }

}

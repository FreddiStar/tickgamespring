package de.almostintelligent.tickgame.controller;

import de.almostintelligent.tickgame.ember.EmberModel;
import de.almostintelligent.tickgame.exception.PlayerNotFoundException;
import de.almostintelligent.tickgame.media.MediaType;
import de.almostintelligent.tickgame.model.*;
import de.almostintelligent.tickgame.service.ConstructibleConditionService;
import de.almostintelligent.tickgame.service.ConstructibleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class ConstructibleController {

    private ConstructibleService constructibleService;
    private ConstructibleConditionService constructibleConditionService;

    @Autowired
    public ConstructibleController(
            ConstructibleService constructibleService,
            ConstructibleConditionService constructibleConditionService) {
        this.constructibleService = constructibleService;
        this.constructibleConditionService = constructibleConditionService;
    }

    @RequestMapping(
            value = "/api/constructibles",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8)
    public EmberModel getConstructibles() throws Exception {

        List<ConstructibleConditionError> errors = new ArrayList<ConstructibleConditionError>();
        Collection<Constructible> constructibles = constructibleService.getConstructibles();
        constructibles.forEach(c -> {
            try {

                constructibleConditionService.isConstructible(c, errors);
            } catch (PlayerNotFoundException e) {
                e.printStackTrace();
            }
        });


        return new EmberModel.Builder(Constructible.class, constructibles)
                .sideLoad(PlayerHasResearch.class, constructibleService.getPlayerHasResearch())
                .sideLoad(ConstructibleConditionError.class, errors)
                .sideLoad(VillageHasBuilding.class, constructibleService.getVillageHasBuilding())
                .sideLoad(ConstructibleHasDependency.class, constructibleService.getConstructibleDependencies())
                .sideLoad(ConstructibleDatum.class, constructibleService.getConstructibleData())
                .build();
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/api/villageHasBuildings",
            produces = MediaType.APPLICATION_JSON_UTF8
    )
    public EmberModel getVillageHasBuilding() throws PlayerNotFoundException {
        return new EmberModel.Builder(VillageHasBuilding.class, constructibleService.getVillageHasBuilding())
                .build();
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/api/playerHasResearches",
            produces = MediaType.APPLICATION_JSON_UTF8
    )
    public EmberModel getPlayerHasResearch() throws PlayerNotFoundException {
        return new EmberModel.Builder(PlayerHasResearch.class, constructibleService.getPlayerHasResearch())
                .build();
    }
}

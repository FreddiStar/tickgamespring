package de.almostintelligent.tickgame.controller;

import de.almostintelligent.tickgame.api.request.StartConstructionRequest;
import de.almostintelligent.tickgame.ember.EmberModel;
import de.almostintelligent.tickgame.exception.ConstructibleNotFoundException;
import de.almostintelligent.tickgame.exception.ConstructionStartFailedException;
import de.almostintelligent.tickgame.exception.PlayerNotFoundException;
import de.almostintelligent.tickgame.media.MediaType;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.service.ConstructibleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ConstructionController {
    private ConstructibleService constructibleService;

    @Autowired
    public ConstructionController(ConstructibleService constructibleService) {
        this.constructibleService = constructibleService;
    }

    @RequestMapping(
            value = "/api/constructions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8)
    public EmberModel getConstructions(@RequestParam(value = "type") String type) throws PlayerNotFoundException {
        return constructibleService.getConstructions(ConstructibleType.forValue(type));
    }

    @RequestMapping(
            value = "/api/constructions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8,
            consumes = MediaType.APPLICATION_JSON_UTF8)
    public EmberModel startConstruction(@RequestBody StartConstructionRequest request)
            throws
            ConstructibleNotFoundException,
            ConstructionStartFailedException,
            PlayerNotFoundException {
        return constructibleService.startConstruction(request);
    }
}

package de.almostintelligent.tickgame.controller;

import de.almostintelligent.tickgame.api.request.RegisterPlayerRequest;
import de.almostintelligent.tickgame.api.response.RegisterPlayerResponse;
import de.almostintelligent.tickgame.exception.EmailAlreadyExistsException;
import de.almostintelligent.tickgame.exception.PasswordsDoNotMatchException;
import de.almostintelligent.tickgame.media.MediaType;
import de.almostintelligent.tickgame.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RegisterController {

    private RegistrationService playerService;

    @Autowired
    public RegisterController(RegistrationService playerService) {
        this.playerService = playerService;
    }

    @RequestMapping(value = "/api/register",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8,
            produces = MediaType.APPLICATION_JSON_UTF8)
    @ResponseStatus(value = HttpStatus.CREATED)
    public RegisterPlayerResponse register(@RequestBody @Valid RegisterPlayerRequest registerAccount) throws PasswordsDoNotMatchException, EmailAlreadyExistsException {
        return playerService.registerPlayer(registerAccount);
    }

}

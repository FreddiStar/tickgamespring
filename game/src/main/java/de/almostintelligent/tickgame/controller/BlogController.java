package de.almostintelligent.tickgame.controller;

import de.almostintelligent.tickgame.ember.EmberModel;
import de.almostintelligent.tickgame.media.MediaType;
import de.almostintelligent.tickgame.model.Blog;
import de.almostintelligent.tickgame.model.BlogComment;
import de.almostintelligent.tickgame.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BlogController {

    private BlogService blogService;

    @Autowired
    public BlogController(BlogService blogService) {
        this.blogService = blogService;
    }

    @RequestMapping(
            value = "/api/blogs",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8
    )
    public EmberModel getBlogs() {
        List<Blog> blogs = blogService.getBlogs();
        return new EmberModel.Builder(Blog.class, blogs)
                .sideLoad(BlogComment.class, blogService.getComments(blogs))
                .build();
    }
}

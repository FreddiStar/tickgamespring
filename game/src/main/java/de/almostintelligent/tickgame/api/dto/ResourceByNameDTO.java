package de.almostintelligent.tickgame.api.dto;

import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.model.VillageHasResource;

import java.util.HashMap;

public class ResourceByNameDTO extends HashMap<String, Double> {

    public static ResourceByNameDTO fromVillage(Village village) {
        ResourceByNameDTO resourceByNameDTO = new ResourceByNameDTO();
        for (VillageHasResource villageHasResource : village.getVillageHasResources()) {
            resourceByNameDTO.put(villageHasResource.getResource().getName(), villageHasResource.getAmount());
        }

        return resourceByNameDTO;
    }

}

package de.almostintelligent.tickgame.api.response.player;

import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;

public class PlayerInfoResponse {
    private String playerName;
    private String focusedVillageName;
    private Long focusedVillageX;
    private Long focusedVillageY;
    private Long playerPoints;
    private Long playerMessageCount;

    public PlayerInfoResponse(Player player, Village focusedVillage, Long playerMessageCount, Long playerPoints) {
        this.playerName = player.getName();
        this.focusedVillageName = focusedVillage.getName();
        this.focusedVillageX = focusedVillage.getX();
        this.focusedVillageY = focusedVillage.getY();
        this.playerPoints = playerPoints;
        this.playerMessageCount = playerMessageCount;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getFocusedVillageName() {
        return focusedVillageName;
    }

    public void setFocusedVillageName(String focusedVillageName) {
        this.focusedVillageName = focusedVillageName;
    }

    public Long getFocusedVillageX() {
        return focusedVillageX;
    }

    public void setFocusedVillageX(Long focusedVillageX) {
        this.focusedVillageX = focusedVillageX;
    }

    public Long getFocusedVillageY() {
        return focusedVillageY;
    }

    public void setFocusedVillageY(Long focusedVillageY) {
        this.focusedVillageY = focusedVillageY;
    }

    public Long getPlayerPoints() {
        return playerPoints;
    }

    public void setPlayerPoints(Long playerPoints) {
        this.playerPoints = playerPoints;
    }

    public Long getPlayerMessageCount() {
        return playerMessageCount;
    }

    public void setPlayerMessageCount(Long playerMessageCount) {
        this.playerMessageCount = playerMessageCount;
    }
}

package de.almostintelligent.tickgame.api.response;

import de.almostintelligent.tickgame.api.dto.ResourceByNameDTO;

public class VillageInfoResponse {

    private ResourceByNameDTO resources;

    public ResourceByNameDTO getResources() {
        return resources;
    }

    public void setResources(ResourceByNameDTO resources) {
        this.resources = resources;
    }
}

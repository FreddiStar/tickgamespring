package de.almostintelligent.tickgame.api.dto;

public class ConstructionDTO {

    private String constructible;

    public String getConstructible() {
        return constructible;
    }

    public void setConstructible(String constructible) {
        this.constructible = constructible;
    }
}

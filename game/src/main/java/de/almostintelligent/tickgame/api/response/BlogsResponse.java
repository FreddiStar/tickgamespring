package de.almostintelligent.tickgame.api.response;

import de.almostintelligent.tickgame.model.Blog;

import java.util.List;

public class BlogsResponse {

    private List<Blog> blog;

    public BlogsResponse(List<Blog> blog) {
        this.blog = blog;
    }

    public List<Blog> getBlog() {
        return blog;
    }

    public void setBlog(List<Blog> blog) {
        this.blog = blog;
    }
}

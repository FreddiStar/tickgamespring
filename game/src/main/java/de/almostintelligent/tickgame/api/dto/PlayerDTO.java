package de.almostintelligent.tickgame.api.dto;

import de.almostintelligent.tickgame.model.Player;

public class PlayerDTO {

    private final String email;
    private final String name;
    private final String playerId;

    public PlayerDTO(Player player) {

        this.playerId = player.getId();
        this.email = player.getEmail();
        this.name = player.getName();

    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPlayerId() {
        return playerId;
    }
}

package de.almostintelligent.tickgame.api.request;

import de.almostintelligent.tickgame.api.dto.ConstructionDTO;

public class StartConstructionRequest {
    private ConstructionDTO construction;

    public ConstructionDTO getConstruction() {
        return construction;
    }

    public void setConstruction(ConstructionDTO construction) {
        this.construction = construction;
    }
}

package de.almostintelligent.tickgame.api.response;

import de.almostintelligent.tickgame.api.dto.PlayerDTO;

public class RegisterPlayerResponse {

    private PlayerDTO player;

    public RegisterPlayerResponse(PlayerDTO player) {
        this.player = player;
    }

    public PlayerDTO getPlayer() {
        return player;
    }

    public void setPlayer(PlayerDTO player) {
        this.player = player;
    }
}

package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.model.Blog;
import de.almostintelligent.tickgame.model.BlogComment;
import de.almostintelligent.tickgame.repository.BlogCommentRepository;
import de.almostintelligent.tickgame.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@Service
public class BlogService {

    private BlogRepository blogRepository;
    private BlogCommentRepository blogCommentRepository;

    @Autowired
    public BlogService(BlogRepository blogRepository, BlogCommentRepository blogCommentRepository) {
        this.blogRepository = blogRepository;
        this.blogCommentRepository = blogCommentRepository;
    }

    public List<Blog> getBlogs() {
        return blogRepository.getBlogsPage();
    }

    public Collection<BlogComment> getComments() {
        return blogCommentRepository.getAll();
    }

    public Collection<?> getComments(List<Blog> blogs) {
        HashSet<BlogComment> comments = new HashSet<>();
        blogs.forEach(blog -> comments.addAll(blog.getComments()));
        return comments;
    }
}

package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.datatypes.ResourcesByName;
import de.almostintelligent.tickgame.model.Village;
import org.springframework.stereotype.Service;

@Service
public class VillageService {

    public boolean villageHasResources(Village village, ResourcesByName resources) {
        ResourcesByName villageResources = ResourcesByName.fromVillage(village);
        return villageResources.hasEnought(resources);
    }

    public void removeResources(Village village, ResourcesByName resourcesByName) {
        village.getVillageHasResources().stream().forEach(villageHasResource -> {
            String resourceName = villageHasResource.getResource().getName();
            if (resourcesByName.containsKey(resourceName)) {
                villageHasResource.setAmount(villageHasResource.getAmount() - resourcesByName.get(resourceName));
            }
        });
    }
}

package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.api.request.StartConstructionRequest;
import de.almostintelligent.tickgame.constructible.ConstructibleDependencyCompliance;
import de.almostintelligent.tickgame.datatypes.ResourcesByName;
import de.almostintelligent.tickgame.ember.EmberModel;
import de.almostintelligent.tickgame.exception.ConstructibleNotFoundException;
import de.almostintelligent.tickgame.exception.ConstructionStartFailedException;
import de.almostintelligent.tickgame.exception.PlayerNotFoundException;
import de.almostintelligent.tickgame.model.*;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;

@Service
public class ConstructibleService {

    private PlayerService playerService;
    private ConstructibleConditionService constructibleConditionService;
    private ConstructibleRepository constructibleRepository;
    private ConstructionRepository constructionRepository;
    private ResourceService resourceService;
    private VillageService villageService;
    private VillageRepository villageRepository;
    private ConstructibleHasDependencyRepository constructibleHasDependencyRepository;
    private ConstructibleDataRepository constructibleDataRepository;
    private ConstructionService constructionService;

    @Autowired
    public ConstructibleService(PlayerService playerService,
                                ConstructibleConditionService constructibleConditionService,
                                ConstructibleRepository constructibleRepository,
                                ConstructionRepository constructionRepository,
                                ResourceService resourceService,
                                VillageService villageService,
                                VillageRepository villageRepository,
                                ConstructibleHasDependencyRepository constructibleHasDependencyRepository,
                                ConstructibleDataRepository constructibleDataRepository,
                                ConstructionService constructionService) {
        this.playerService = playerService;
        this.constructibleConditionService = constructibleConditionService;
        this.constructibleRepository = constructibleRepository;
        this.constructionRepository = constructionRepository;
        this.resourceService = resourceService;
        this.villageService = villageService;
        this.villageRepository = villageRepository;
        this.constructibleHasDependencyRepository = constructibleHasDependencyRepository;
        this.constructibleDataRepository = constructibleDataRepository;
        this.constructionService = constructionService;
    }

    public EmberModel startConstruction(StartConstructionRequest request)
            throws
            ConstructibleNotFoundException,
            ConstructionStartFailedException, PlayerNotFoundException {

        Constructible constructible = constructibleRepository.findOne(request.getConstruction().getConstructible());

        if (constructible == null) {
            throw new ConstructibleNotFoundException();
        }

        if (!constructibleConditionService.isConstructible(constructible, null)) {
            throw new ConstructionStartFailedException();
        }

        Village village = playerService.focusedVillage();
        Player player = playerService.player();

        Long nextLevel = 0L;

        if (constructible.getType().equals(ConstructibleType.BUILDING)) {
            nextLevel = getNextLevelOfConstructible(constructible, village.getVillageHasBuildingsMap());
        } else if (constructible.getType().equals(ConstructibleType.RESEARCH)) {
            nextLevel = getNextLevelOfConstructible(constructible, player.getPlayerHasResearchesMap());
        } else if (constructible.getType().equals(ConstructibleType.UNIT)) {
            nextLevel = 1L;
        }

        ResourcesByName cost = resourceService.getConstructibleCostForLevel(constructible, nextLevel);
        villageService.removeResources(village, cost);
        village = villageRepository.save(village);

        Construction construction = new Construction();
        construction.setConstructible(constructible);
        construction.setPlayer(player);

        if (isBuilding(constructible) || isUnit(constructible)) {
            construction.setVillage(village);
        }

        construction.setTicksLeft(getTicksLeft(constructible, nextLevel, village, player));
        construction.setAmount(0L);

        construction = constructionRepository.save(construction);

        return new EmberModel.Builder(Construction.class, construction)
                .sideLoad(Village.class, village, true)
                .sideLoad(Player.class, player, true)
                .build();
    }

    private static boolean isBuilding(Constructible c) {
        return c.getType().equals(ConstructibleType.BUILDING);
    }

    private static boolean isResearch(Constructible c) {
        return c.getType().equals(ConstructibleType.RESEARCH);
    }

    private static boolean isUnit(Constructible c) {
        return c.getType().equals(ConstructibleType.UNIT);
    }

    private Long getNextLevelOfConstructible(Constructible constructible, Map<String, ? extends ConstructibleDependencyCompliance> complianceMap) {
        return getLevelOfConstructible(constructible, complianceMap) + 1;
    }

    private Long getLevelOfConstructible(Constructible constructible, Map<String, ? extends ConstructibleDependencyCompliance> complianceMap) {
        ConstructibleDependencyCompliance compliance = complianceMap.get(constructible.getId());

        if (compliance != null) {
            return compliance.getLevel();
        }

        return 0L;
    }

    private Long getTicksLeft(Constructible constructible, Long level, Village village, Player player) {
        ConstructibleDatum data = constructible.getConstructibleDatas().get(ConstructibleDatum.CONSTRUCTION_TICK_COUNT);

        if (isUnit(constructible)) {
            Long maxTicksLeft = constructionRepository.maxTicksLeft(village, player, ConstructibleType.UNIT);
            if (null == maxTicksLeft) {
                maxTicksLeft = 0L;
            }

            return maxTicksLeft + data.grow(level).longValue();
        }

        return data.grow(level).longValue();
    }

    public Collection<Constructible> getConstructibles() {
        return constructibleRepository.getAll();
    }

    public Collection<PlayerHasResearch> getPlayerHasResearch() throws PlayerNotFoundException {
        return playerService.player().getPlayerHasResearches();
    }

    public Collection<VillageHasBuilding> getVillageHasBuilding() throws PlayerNotFoundException {
        return playerService.focusedVillage().getVillageHasBuildings();
    }

    public Collection<ConstructibleHasDependency> getConstructibleDependencies() {
        return constructibleHasDependencyRepository.getAll();
    }

    public Collection<ConstructibleDatum> getConstructibleData() {
        return constructibleDataRepository.getAll();
    }

    public EmberModel getResearchConstructions() throws PlayerNotFoundException {
        Player player = playerService.player();
        return new EmberModel.Builder(Construction.class, constructionRepository.getConstructionsByPlayerAndType(player, ConstructibleType.RESEARCH))
                .sideLoad(Player.class, player, true)
                .build();
    }

    public EmberModel getBuildingConstructions() throws PlayerNotFoundException {
        Player player = playerService.player();
        Village focusedVillage = playerService.focusedVillage();
        return new EmberModel.Builder(Construction.class, constructionRepository.getConstructionsByVillageAndPlayerAndType(focusedVillage, player, ConstructibleType.BUILDING))
                .sideLoad(Village.class, focusedVillage, true)
                .sideLoad(Player.class, player, true)
                .build();
    }

    public EmberModel getConstructions(ConstructibleType type) throws PlayerNotFoundException {
        switch (type) {
            case BUILDING:
                return getBuildingConstructions();
            case RESEARCH:
                return getResearchConstructions();
            case UNIT:
                return null;
        }

        return null;
    }
}

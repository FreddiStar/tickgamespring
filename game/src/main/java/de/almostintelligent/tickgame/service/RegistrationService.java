package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.api.dto.PlayerDTO;
import de.almostintelligent.tickgame.api.request.RegisterPlayerRequest;
import de.almostintelligent.tickgame.api.response.RegisterPlayerResponse;
import de.almostintelligent.tickgame.exception.EmailAlreadyExistsException;
import de.almostintelligent.tickgame.exception.PasswordsDoNotMatchException;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.PlayerActivation;
import de.almostintelligent.tickgame.repository.PlayerActivationRepository;
import de.almostintelligent.tickgame.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RegistrationService {

    private PlayerRepository playerRepository;
    private PlayerActivationRepository playerActivationRepository;

    @Autowired
    public RegistrationService(PlayerRepository playerRepository,
                               PlayerActivationRepository playerActivationRepository) {
        this.playerRepository = playerRepository;
        this.playerActivationRepository = playerActivationRepository;
    }

    public RegisterPlayerResponse registerPlayer(RegisterPlayerRequest registerAccount) throws PasswordsDoNotMatchException, EmailAlreadyExistsException {
        if (!registerAccount.getPassword().equals(registerAccount.getPasswordRepeat())) {
            throw new PasswordsDoNotMatchException();
        }

        if (null != playerRepository.findOneByEmail(registerAccount.getEmail())) {
            throw new EmailAlreadyExistsException();
        }

        Player player = new Player();
        player.setEmail(registerAccount.getEmail());
        player.setPassword(BCrypt.hashpw(registerAccount.getPassword(), BCrypt.gensalt()));

        PlayerActivation playerActivation = new PlayerActivation();
        playerActivation.setPlayer(player);
        playerActivation.setToken(UUID.randomUUID().toString());

        player = playerRepository.save(player);

        playerActivationRepository.save(playerActivation);

        return new RegisterPlayerResponse(new PlayerDTO(player));
    }
}

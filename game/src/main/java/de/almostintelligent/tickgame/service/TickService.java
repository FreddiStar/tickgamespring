package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.tick.RegisterAsTickAction;
import de.almostintelligent.tickgame.tick.TickAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class TickService {

    public static final int TICK_ACTION_WEIGHT_RESOURCES = 0;
    public static final int TICK_ACTION_WEIGHT_BUILDING_CONSTRUCTION = 10;
    public static final int TICK_ACTION_WEIGHT_RESEARCH_CONSTRUCTION = 11;
    public static final int TICK_ACTION_WEIGHT_UNIT_CONSTRUCTION = 12;
    public static final int TICK_ACTION_WEIGHT_UPATE_POINTS = 20;

    private Logger logger = LoggerFactory.getLogger(TickService.class.getName());
    private ListableBeanFactory listableBeanFactory;
    private List<TickAction> tickActions;

    @Autowired
    public TickService(ListableBeanFactory listableBeanFactory) {
        this.listableBeanFactory = listableBeanFactory;
    }

    @PostConstruct
    public void initTickActions() {
        tickActions = new ArrayList<TickAction>();
        getTickActions().stream().forEach(a -> tickActions.add((TickAction) a));
        tickActions.sort((o1, o2) -> o1.weight().compareTo(o2.weight()));

    }

    // every 10th minute
    //@Scheduled(cron = "0 */10 * * * *")

    // every 10th second
    @Scheduled(cron = "*/10 * * * * *")
    public void doTick() {
        logger.debug("Tick Started");

        for (TickAction action : tickActions) {
            logger.trace("TickAction " + action.name());
            action.doAction();
        }

        logger.debug("Tick Ended");
    }

    private Collection<Object> getTickActions() {
        return listableBeanFactory.getBeansWithAnnotation(RegisterAsTickAction.class).values();
    }

}

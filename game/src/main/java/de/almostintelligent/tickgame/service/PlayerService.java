package de.almostintelligent.tickgame.service;

import com.google.common.collect.Lists;
import de.almostintelligent.tickgame.api.response.player.PlayerInfoResponse;
import de.almostintelligent.tickgame.exception.LoginFailedException;
import de.almostintelligent.tickgame.exception.PlayerNotFoundException;
import de.almostintelligent.tickgame.model.Player;
import de.almostintelligent.tickgame.model.Village;
import de.almostintelligent.tickgame.repository.PlayerRepository;
import de.almostintelligent.tickgame.repository.VillageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.logging.Logger;


@Service
public class PlayerService {

    Logger logger = Logger.getLogger(PlayerService.class.getSimpleName());

    private VillageRepository villageRepository;
    private PlayerRepository playerRepository;

    @Autowired
    public PlayerService(VillageRepository villageRepository, PlayerRepository playerRepository) {
        this.villageRepository = villageRepository;
        this.playerRepository = playerRepository;
    }

    public Player player() throws PlayerNotFoundException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext == null) {
            failedAuthentication("no security context found");
        }

        Authentication authentication = securityContext.getAuthentication();
        if (authentication == null) {
            failedAuthentication("no authentication found");
        }

        UserDetails principal;
        if (authentication.getPrincipal() instanceof UserDetails) {
            principal = (UserDetails) authentication.getPrincipal();
        } else {
            throw new PlayerNotFoundException();
        }

        return playerRepository.findOneByEmail(principal.getUsername());
    }

    public Village focusedVillage() throws PlayerNotFoundException {
        return villageRepository.findByPlayerAndFocused(player(), true);
    }

    private void failedAuthentication(String msg) {
        logger.warning("failed authentication: " + msg);
        throw new LoginFailedException(msg);
    }

    public PlayerInfoResponse getPlayerInfo() throws PlayerNotFoundException {
        return new PlayerInfoResponse(player(), focusedVillage(), 0L, 0L);
    }

    public Collection<Player> getAllPlayers() {
        return Lists.newArrayList(playerRepository.findAll());
    }
}

package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.constructible.condition.ConstructibleCondition;
import de.almostintelligent.tickgame.constructible.condition.RegisterAsConstructibleCondition;
import de.almostintelligent.tickgame.exception.PlayerNotFoundException;
import de.almostintelligent.tickgame.model.*;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
public class ConstructibleConditionService {

    private ListableBeanFactory listableBeanFactory;
    private PlayerService playerService;
    private List<ConstructibleCondition> conditions;

    @Autowired
    public ConstructibleConditionService(ListableBeanFactory listableBeanFactory,
                                         PlayerService playerService) {
        this.listableBeanFactory = listableBeanFactory;
        this.playerService = playerService;
    }

    @PostConstruct
    public void initHandlers() {
        conditions = new ArrayList<ConstructibleCondition>();
        getConditions().stream().forEach(c -> add((ConstructibleCondition) c));
    }

    private void add(ConstructibleCondition c) {
        conditions.add(c);
    }

    /**
     * Checks if constructible is constructible
     *
     * @param constructible Constructible to check
     * @param errors        errors
     * @return true if constructible is constructible
     */
    public boolean isConstructible(Constructible constructible, List<ConstructibleConditionError> errors) throws PlayerNotFoundException {
        return isConstructible(constructible, playerService.focusedVillage(), playerService.player(), errors);
    }

    public boolean isConstructible(Constructible constructible, Village village, Player player, List<ConstructibleConditionError> errors) {
        boolean result = true;

        Map<String, VillageHasBuilding> villageHasBuildings = village.getVillageHasBuildingsMap();

        Long level = 1L;
        VillageHasBuilding villageHasBuilding = villageHasBuildings.get(constructible.getId());
        if (null != villageHasBuilding) {
            level = villageHasBuilding.getLevel() + 1;
        }

        for (ConstructibleCondition condition : conditions) {
            if (condition.type().contains(constructible.getType())) {
                if (!condition.meet(constructible, level, village, player)) {
                    result = false;
                    if (null != errors) {
                        errors.add(new ConstructibleConditionError(condition.id(), condition.error(), constructible));
                    }
                }
            }
        }

        return result;
    }

    private Collection<Object> getConditions() {
        return listableBeanFactory.getBeansWithAnnotation(RegisterAsConstructibleCondition.class).values();
    }

}

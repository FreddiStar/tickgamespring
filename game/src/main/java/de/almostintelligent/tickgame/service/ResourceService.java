package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.datatypes.ResourcesByName;
import de.almostintelligent.tickgame.model.Constructible;
import de.almostintelligent.tickgame.model.ConstructibleDatum;
import org.springframework.stereotype.Service;

@Service
public class ResourceService {
    public ResourcesByName getConstructibleCostForLevel(Constructible constructible, Long level) {

        ResourcesByName result = new ResourcesByName();

        for (ConstructibleDatum datas : constructible.getConstructibleDatas().values()) {
            if (datas.getName().startsWith("COST_")) {
                Double cost = datas.grow(level);
                result.put(datas.getName().split("_")[1].toLowerCase(), cost);
            }
        }

        return result;

    }
}

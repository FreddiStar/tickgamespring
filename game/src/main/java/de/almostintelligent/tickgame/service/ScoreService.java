package de.almostintelligent.tickgame.service;

import de.almostintelligent.tickgame.model.*;
import de.almostintelligent.tickgame.model.types.ConstructibleType;
import de.almostintelligent.tickgame.model.types.ScoreType;
import de.almostintelligent.tickgame.repository.PlayerScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ScoreService {

    private PlayerService playerService;
    private PlayerScoreRepository playerScoreRepository;

    @Autowired
    public ScoreService(PlayerService playerService,
                        PlayerScoreRepository playerScoreRepository) {
        this.playerService = playerService;
        this.playerScoreRepository = playerScoreRepository;
    }

    @Transactional
    public void updateScore() {
        playerScoreRepository.deleteAll();

        playerService.getAllPlayers().forEach(this::updatePlayerScore);
    }

    private void updatePlayerScore(Player player) {
        updatePlayerResearchScore(player);
        updatePlayerUnitScore(player);
        player.getVillages().forEach(village -> updatePlayerVillageScore(player, village));
    }

    private void updatePlayerResearchScore(Player player) {
        Double score = 0.0;
        for (PlayerHasResearch playerHasResearch : player.getPlayerHasResearches()) {
            score += getConstructibleScore(playerHasResearch.getConstructible(), playerHasResearch.getLevel());
        }

        Score playerScore = new Score();
        playerScore.setScore(score);
        playerScore.setPlayer(player);
        playerScore.setType(ScoreType.RESEARCH);
        playerScoreRepository.save(playerScore);
    }

    private void updatePlayerUnitScore(Player player) {
        Double score = 0.0;
        for (PlayerHasUnit playerHasUnit : player.getPlayerHasUnits()) {
            score += getConstructibleScore(playerHasUnit.getConstructible(), 1L);
        }
        Score playerScore = new Score();
        playerScore.setScore(score);
        playerScore.setPlayer(player);
        playerScore.setType(ScoreType.UNIT);
        playerScoreRepository.save(playerScore);
    }

    private void updatePlayerVillageScore(Player player, Village village) {
        Double score = 0.0;
        for (VillageHasBuilding villageHasBuilding : village.getVillageHasBuildings()) {
            score += getConstructibleScore(villageHasBuilding.getConstructible(), villageHasBuilding.getLevel());
        }

        Score villageScore = new Score();
        villageScore.setScore(score);
        villageScore.setPlayer(player);
        villageScore.setVillage(village);
        villageScore.setType(ScoreType.BUILDING);
        playerScoreRepository.save(villageScore);
    }

    private Double getConstructibleScore(Constructible constructible, Long level) {
        if (constructible.getType().equals(ConstructibleType.UNIT)) {

        } else {

        }
        return Double.valueOf(level);
    }
}

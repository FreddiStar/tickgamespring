package de.almostintelligent.tickgame.conversion;

import de.almostintelligent.tickgame.api.dto.PlayerDTO;
import de.almostintelligent.tickgame.model.Player;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DTOConversionTest {

    @Test
    public void shouldCreatePlayerDTO() {
        Player player = new Player();
        player.setName("name");
        player.setEmail("email");
        player.setId("someid");

        PlayerDTO playerDTO = new PlayerDTO(player);

        assertThat(playerDTO.getPlayerId(), is("someid"));
        assertThat(playerDTO.getName(), is("name"));
        assertThat(playerDTO.getEmail(), is("email"));

    }

}

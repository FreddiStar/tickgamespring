# Dokumentation

Technische Dokumentation von TickGame

## Packages
### de.almostintelligent.tickgame
#### .api
Alles was mit der REST-API zu tun hat
#### .controller
Alle Controller. Hier sollte keine Logic implementiert werden.
#### .exception
Exceptions
#### .fixture
Alle Fixtures des Spiels
#### .media
Alle Klassen rund um Medien
#### .model
Datenbank und anderen Models
#### .repository
Repositories für die Models
#### .service
Services die die Logic enthalten sollten

## OAuth2
